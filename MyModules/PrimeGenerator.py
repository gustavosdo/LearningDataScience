import math
class PrimeGenerator(object):
    def generate_primes(self, max_num):
        array = [False, False, True] + [None for i in range(3, max_num)]
        for i in array:
            if i == None:
                i = array.index(i)
                for j in range(2,i):
                    if i%j == 0:
                        array[i] = False
                        break
                    else:
                        if (j==i-1):
                            array[i] = True
                            self._cross_off(array,i)
        return(array)

    def _cross_off(self, array, prime):
        n = 2
        while True:
            try:
                N = n*prime
                array[N] = False
                n += 1
            except IndexError:
                break

prime_generator = PrimeGenerator()

import time
start = time.process_time()
prime_generator.generate_primes(100)
print("Time to find primes until 100", time.process_time() - start)

start = time.process_time()
prime_generator.generate_primes(1000)
print("Time to find primes until 1k", time.process_time() - start)

start = time.process_time()
prime_generator.generate_primes(10000)
print("Time to find primes until 10k", time.process_time() - start)

start = time.process_time()
prime_generator.generate_primes(100000)
print("Time to find primes until 100k", time.process_time() - start)

start = time.process_time()
prime_generator.generate_primes(1000000)
print("Time to find primes until 1M", time.process_time() - start)
