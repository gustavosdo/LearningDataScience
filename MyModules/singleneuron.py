# -*- coding: utf-8 -*-
# author: gustavo.if.ufrj@gmail.com

class SingleNeuron():
    
    def __init__(self, inputs, weights, bias = -1,
                 activate_function = "step", linear_constant = 1):
        self.inpts = inputs
        self.wgts = weights
        self.bias = bias
        self.activate_function = activate_function
        self.linear_constant = linear_constant
    
    def output(self):
        """
        Apply the activation function to the sum of products of inputs
        and weights and returns its output value if the sum is greater than
        the bias value.
        
        Parameters
        ----------
        inputs : A native python/numpy array with the values of each input.
        
        weights : A native python/numpy array with the values of each weight
        with the same length of inputs parameter.
        
        bias : Mathematically, bias = - threshold, i.e., the greater the value
        of threshold smaller the probability of neuron emits a non-zero output.
        Defaults to -1.
        
        active_function : Activation function. Defaults to step (Heaviside)
        function. Possible options: "step", "sigmoid", "linear", "tanh", "relu"
        or "leaky".
        
        linear_constant : If the choosen activate function is either linear, relu,
        or leaky than this parameter is used as the constant in the linear
        relantionship. Defaults to 1.
        
        Returns
        -------
        The activation function output using the sum of products 
        
        Improvements to be developed
        -------
        i) Check length of inputs and weights
        ii) Add try-except in type conversion
        iii) Implement softmax activation function
        """
        
        # Loading required libraries
        from math import exp, tanh
        import numpy as np
        
        # Defining activation functions ---------------------------------------
        if (self.activate_function == "step"):
            def actvfunc(x):
                if (x >= 0): return(1)
                return(0)
        elif (self.activate_function == "sigmoid"):
            def actvfunc(x):
                return(1./(1. + exp(-x)))
        elif (self.activate_function == "tanh"):
            def actvfunc(x):
                return(tanh(x))
        elif (self.activate_function == "linear"):
            def actvfunc(x, c = self.linear_constant):
                return(c*x)
        elif (self.activate_function == "relu"):
            def actvfunc(x, c = self.linear_constant):
                if (x >= 0): return(c*x)
                return(0)
        elif (self.activate_function == "leaky"):
            def actvfunc(x, c = self.linear_constant):
                if (x >= 0): return(x)
                return(c*x)
        #elif (activate_function == "softmax"):
        #    def actvfunc(x, bias)
        
        # Converting inputs and weights if needed -----------------------------
        if (type(self.inpts) != np.ndarray): self.inpts = np.array(self.inpts)
        if (type(self.wgts) != np.ndarray): self.wgts = np.array(self.wgts)
        
        # Output of activate function with the input from inputs and weights
        return(actvfunc(x = np.sum(self.inpts*self.wgts) + self.bias))