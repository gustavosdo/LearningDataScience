# -*- coding: utf-8 -*-
# author: gustavo.if.ufrj@gmail.com

def movingmean(timeseries, lag = None): # -------------------------------------
    """
    Apply moving mean method to a time series

    Parameters
    ----------
    timeseries : A native python/numpy array of float numbers
    lag : number of items to be considered for the mean calculation.
        Defaults to None. If None is determined as 5% of sample size.

    Returns
    -------
    mmts : Acronymn for moving mean time series, it is an object of the same
        type as timeseries
    """
    
    # Loading required libraries
    import numpy as np
    
    # Converting sample object if needed
    if (type(timeseries) != np.ndarray): timeseries = np.array(timeseries)
    
    # Defining lag if it was not defined before
    if (lag == None): lag = int(np.ceil(0.05*len(timeseries)))
    
    # Creating the resultante time series
    mmts = np.zeros(len(timeseries))
    
    # Looping over time series items-------------------------------------------
    for order in range(len(timeseries)):
        # Defining minimum order of sample to be considered in mean calculation
        min_order = order - lag
        if (min_order < 0): min_order = 0 # preventing use of negative orders
        
        # Defining maximum order of sample to be considered in mean calculation
        max_order = order + lag + 1
        # preventing use of orders greater than length of sample (possible NA)
        if (max_order > len(timeseries)): max_order = len(timeseries)
        
        # Determining the mean value for each order of sample
        mmts[order] = np.mean(timeseries[min_order:max_order])
    # Close loop of timeseries-------------------------------------------------

    # returning the result
    return(mmts)