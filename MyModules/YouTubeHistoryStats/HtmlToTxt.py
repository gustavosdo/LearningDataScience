import codecs
import html2text

def convert(html):
    YThistory = codecs.open(html+'.html','r', 'utf-8')
    YThtml = YThistory.read()
    h = html2text.HTML2Text()
    h.ignore_links = True
    YTtxt = html2text.html2text(YThtml)

    with open(html+'.txt', 'w') as text:
        text.write(YTtxt)

convert('MyHistory')
