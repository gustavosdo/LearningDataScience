import math

class PrimeGenerator(object):
    def generate_primes(self, max_num):
        allInts = []
        while True:
            try:
                allInts = [n for n in range(0, max_num)]
                for n in allInts:
                    self._next_prime(allInts, n)
                break
            except:
                print("Not an int!")
                allInts = None
                break
        return allInts
            
    def _next_prime(self, array, prime):
        # Implemente aqui sua solução
        if prime in [0,1]:
            array[array.index(prime)] = False
        elif prime == 2:
            array[array.index(prime)] = True
        else:
            self._cross_off(array, prime)

    def _cross_off(self, array, prime):
        # Implemente aqui sua solução
        restos_prime = [prime%j for j in range(2,prime)]
        if 0 in restos_prime:
            array[array.index(prime)] = False
        else:
            array[array.index(prime)] = True

primes341 = PrimeGenerator()
listPrimesUntil341 = primes341.generate_primes(341)
print(listPrimesUntil341)
