from functools import reduce

words = ['Ovo', 'ouvido', 'avenida', 'COMPRIMENTO', 'EnTrEtEnImEnTo']

print(reduce(lambda a,b: a if (len(a) > len(b)) else b, words).capitalize())
