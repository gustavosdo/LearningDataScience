# -*- coding: utf-8 -*-

from singleneuron import SingleNeuron
from random import random as rndm
#import numpy as np

# Test with XOR problem when backpropagation is done
#inputs = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
#output = np.array([0, 1, 1, 0])

class nLayersNN():
    
    def __init__(self, nlayers = 2, neurons_layer = [2, 2], bias = [-1, -1],
                 inputs = None, output = None, activate_function = "step",
                 input_bias = -1):
        """
        A module to define a neural network using single neurons
        and gradient descent model in a backpropagation context.
        
        Parameters
        ----------
        nlayers : Mandatory integer defining the number of layers (input layer
                and output layer excludeds) of the neural network. The
                default is 2.
                
        neurons_layer : Mandatory integer array defining the number of neuron
                        per layer of the neural network.
                        The default is [2, 2, 2].
                        
        inputs : A python/numpy array with the numerical inputs.
        
        output : A single expected numerical output.

        Returns
        -------
        The set of learnt weights per neuron and input.
        
        Improvements to be developed
        -------
        - Check consistency of inputs (e.g. same length for all items in inputs)
        - Allow lists for activate_function
        """        
        # Defining class parameters using initial variables -------------------
        self.nlayers = nlayers
        if (len(neurons_layer) == nlayers):
            self.neurons_layer = neurons_layer
        elif ((type(neurons_layer) == int) | (type(neurons_layer) == float)):
            self.neurons_layer = [neurons_layer for n in range(nlayers)]
        self.inputs = inputs
        if (len(bias) == len(neurons_layer)):
            self.bias = bias
        elif ((type(bias) == int) | (type(bias) == float)):
            self.bias = [bias for n in neurons_layer]
        self.output = output
        self.activate_function = activate_function
        
        # Adding bis for the input layer
        self.bias.insert(0, input_bias)
        
        # Adding output layer
        self.neurons_layer.append(1)
        self.nlayers = self.nlayers + 1        
           
    def wgtstensor(self):
        # Starting weights tensor
        wgts = []
        
        # Inputs for the first layer are the inputs given as parameter
        inputs = self.inputs
        
        # Looping over layers of neural network -------------------------------
        for layer in range(self.nlayers):
            
            # Number of neurons for the present layer
            neurons = self.neurons_layer[layer]
            
            # Starting random weights
            wgts.append([[rndm() for n in range(len(inputs))]
                         for m in range(neurons)])
            
            # Updating input: a layer feeds the next one and the input and
            # weights have the same length
            inputs = wgts[layer]
        # End loop of neural network layers -----------------------------------
            
        # Tensor of weights is returned
        return(wgts)
    
    def feedforward(self):
        wgtstensor = self.wgtstensor()
        inputs = self.inputs
        for layer in range(self.nlayers):
            bias = self.bias[layer]
            wgts = wgtstensor[layer]
            neurons = self.neurons_layer[layer]
            res = []
            for neuron in range(neurons):
                res.append(SingleNeuron(inputs = inputs,
                                        weights = wgts[neuron],
                                        bias = bias,
                                        activate_function = self.activate_function).output())
            # End loop of neurons
            inputs = res
            print(res)     
        # End of loop of layers
        return(res)
        