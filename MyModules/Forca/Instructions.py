class Instructions():
    def __init__(self):
        pass

    def Print(self):
        print(' * * * * * Bem-vindo ao jogo da forca * * * * * ')
        print(' - - Instruções:')
        print(' - Escolha de 1 a 4 jogadores')
        print(' - Defina o nome de cada jogador')
        print(' - Cada jogador tenta uma letra para a palavra escolhida pelo jogo')
        print(' - Se o jogador errar um dos membros do seu boneco é desenhado')
        print(' - Ao todo ssão sete membros')
        print(' - Após o desenho ficar completo o jogador perde')
        print(' - Cada vez que acerta uma letra o jogador ganha um ponto e continua tentando outra letra')
        print(' - Dos jogadores que restarem no final aquele com maior pontuação vence')
        print(' - É possível pedir para dar um palpite ao invés de tentar uma letra')
        print(' - Neste caso é tudo ou nada: se o jogador acerta, ganha mas se errar perde e o jogo continua')
        print(' * * * * * * * * * * * * * * * * * * * * * * * *')
