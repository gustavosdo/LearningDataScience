# Importing library to choose word randomly
import random
# Importing ordered dict
from collections import OrderedDict
# Importing drawings of hangman game
from Graphics import *
# Importing instructions
from Instructions import *

class HangmanGame():
    def __init__(self, nPlayers = 0, namePlayers = [], pointPlayers = {}, errorPlayers = {},\
                 word = '', maskWord = '', listMaskWord = '', usedLetters = [], tryLetter = '',\
                 player = '', shotWords = [], shot = ''):
        self.nPlayers = nPlayers
        self.namePlayers = namePlayers
        self.pointPlayers = pointPlayers
        self.errorPlayers = errorPlayers
        self.word = word
        self.maskWord = maskWord
        self.listMaskWord = listMaskWord
        self.usedLetters = usedLetters
        self.tryLetter = tryLetter
        self.player = player
        self.shotWords = shotWords
        self.shot = shot
        gameInstructions = Instructions()
        gameInstructions.Print()

    def NumberOfPlayers(self):
        while True:
            try:
                self.nPlayers = int(input("Digite o Número de Jogadores (entre 1 e 4): "))
                if (self.nPlayers > 1) or (self.nPlayers < 1):
                    print ("Número de jogadores inválido")
                    continue
            except:
                print ("Você não digitou um número!")
                continue
            else:
                print("Número de jogadores: %d" %self.nPlayers)
                break

    def NameOfPlayers(self):
        points = []
        for n in range(0,self.nPlayers):
            name = str(input("Digite o número do jogador %d: " %(n+1)))
            self.namePlayers.append(name)
            points.append(0)
        random.shuffle(self.namePlayers)
        self.pointPlayers = OrderedDict(zip(self.namePlayers, points))
        self.errorPlayers = OrderedDict(zip(self.namePlayers, points))
        #print("O jogador %s é o primeiro a jogar!" %self.player)        

    def ChooseWord(self):
        with open('palavras.txt', 'r') as fileWords:
            nLines = sum(1 for line in fileWords)
            nWord = random.choice([x for x in range(0, nLines-1)])
            fileWords.seek(0)
            self.word = fileWords.readlines()[nWord]
            for c in range(1, len(self.word)):
                self.maskWord += '?'
        self.listMaskWord = list(self.maskWord)
        print("A palavra escolhida tem %d letras" %(len(self.word)-1))
        print("%s é a palavra escolhida" %self.maskWord)

    def TryLetter(self):
        while True:
            print(' * * * * * * * * * * * * * * * * * * * * * * * *')
            print("Letras já usadas: ", self.usedLetters)
            print(desenhos[self.errorPlayers[self.player]])
            self.tryLetter = str(input("%s, digite uma letra: " %self.player)).lower()
            if (self.tryLetter.isdigit()):
                print("Você digitou um número!")
                continue
            elif (self.tryLetter.isalnum() and self.tryLetter.isalpha() == False):
                print("Você digitou uma mistura de letras e números!")
                continue
            elif (len(self.tryLetter) != 1):
                print("Você digitou mais (ou menos) de uma letra!")
                continue
            elif (self.tryLetter == ' '):
                print("Você digitou apenas um espaço!")
                continue
            elif (self.tryLetter in self.usedLetters):
                print("Já tentaram essa letra")
                continue
            else:
                print("%s tentou a letra %s" %(self.player, self.tryLetter))
                self.usedLetters.append(self.tryLetter)
                break
        isRight = 0
        for i in range(0, len(self.word)):
            if self.tryLetter == list(self.word)[i]:
                self.listMaskWord[i] = self.tryLetter
                isRight += 1
        if (isRight > 0):
            self.pointPlayers[self.player] += 1
            print('Você acertou a letra %d vez(es) =)' %isRight)
        else:
            self.errorPlayers[self.player] += 1
            print('Não existe %s na palavra escolhida' %self.tryLetter)
        self.maskWord = ''.join(self.listMaskWord)
        print('%s é a palavra escolhida' %self.maskWord)
        print(' * * * * * * * * * * * * * * * * * * * * * * * *')
            

    def Shot(self):
        while True:
            shot = str(input('Qual é a palavra?'))
            confirm = str(input('Está certo disso? (sim ou não)')).lower()
            if confirm == 'sim':
                self.shot = shot
                break
            elif confirm == 'não':
                continue
            else:
                print("Não entendi... digite sim ou não")
                continue
        print("A palavra é %s" %self.word)
        if ((self.shot == self.word) or (self.shot+'\n' == self.word)):
            return True
        else:
            print(self.word)
            print(len(self.word))
            print(shot)
            print(len(self.shot))
            return False

def TryOrShoot():    
    while True:
        try:
            digit = int(input("Digite 1 para tentar uma letra e 2 para tentar uma palavra: "))
            if (digit < 1) or (digit > 2):
                print("Opção inválida")
                continue
        except:
            print("Você não digitou um número")
            continue
        else:
            return digit
            break
    #self.nPlayers = nPlayers
    #self.namePlayers = namePlayers
    #self.pointPlayers = pointPlayers
    #self.errorPlayers = errorPlayers
    #self.word = word
    #self.maskWord = maskWord
    #self.listMaskWord = listMaskWord
    #self.usedLetters = usedLetters
    #self.tryLetter = tryLetter
    #self.player = player
    #
    #    def Desenho(erros):
    #        print(desenhos[erros])
    #
