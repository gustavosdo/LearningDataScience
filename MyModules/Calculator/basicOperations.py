Soma = lambda x,y: x+y
Subt = lambda x,y: x-y
Mult = lambda x,y: x*y
Divi = lambda x,y: x/y

def printOptions():
    print("\nOperações disponíveis:")
    print("\n1 - Soma")
    print("\n2 - Subtração")
    print("\n3 - Multiplicação")
    print("\n4 - Divisão")
    print("\n5 - SAIR")

print("\n******************* Calculadora Python *******************")

Opcoes = [1, 2, 3, 4, 5]
opcao = 0
while opcao != 5:
    while opcao not in Opcoes:
        printOptions()
        opcao = int(input('\nEscolha a operação: '))
        if opcao not in Opcoes: print('\nESCOLHA UMA OPÇÃO DE 1 A 5!')

    if opcao == 5: sys.exit()

    alg1 = int(input('\nDigite o primeiro número: '))
    alg2 = 0.
    if opcao == 4:
        while alg2 == 0.:
            alg2 = int(input('\nDigite o segundo número (!= 0): '))
    elif opcao > 0 and opcao < 4:
        alg2 = int(input('\nDigite o segundo número: '))

    if opcao == 1:
        print("%f + %f = %f" %(alg1, alg2, Soma(alg1,alg2)))
        opcao = 0
    if opcao == 2:
        print("%f - %f = %f" %(alg1, alg2, Subt(alg1,alg2)))
        opcao = 0
    if opcao == 3:
        print("%f x %f = %f" %(alg1, alg2, Mult(alg1,alg2)))
        opcao = 0
    if opcao == 4:
        print("%f / %f = %f" %(alg1, alg2, Divi(alg1,alg2)))
        opcao = 0
