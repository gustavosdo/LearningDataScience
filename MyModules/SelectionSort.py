# this function was designed to build a list
# with unique values from a list with a large number of
# each value and return the number of times the
# repeated items are present
def getUniqueValuesList(someList):
	timesPerItem = dict.fromkeys(someList, 1)
	uniqueValuesList = []
	for item in someList:
		if (item in uniqueValuesList): timesPerItem[item] += 1
		else: uniqueValuesList.append(item)
	return( [uniqueValuesList, timesPerItem] )

data = [5, 1, 7, 2, 6, -3, 5, 7, -10]

[data,times] = getUniqueValuesList(data)

minor = 0

for j in range(0,len(data)-1):
	minor = data[j]
	for i in range(j,len(data)):
		minor = min(data[i],minor)
		print("minor: ", minor)
	print("minor (out of loop): ", minor)
	data.remove(minor)
	data.insert(j,minor)
	print("data: ", data)

for value,times in times.items():
	j = data.index(value)
	for i in range(1,times):
		data.insert(j,value)

print("data: ", data)
