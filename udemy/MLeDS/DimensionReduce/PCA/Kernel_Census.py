import pandas as pd
base = pd.read_csv('../../NaiveBayes/Census/census.csv')
#previsores = base.iloc[:,0:14].values # MemoryError
#classe = base.iloc[:,14].values
previsores = base.iloc[0:5000,0:14].values
classe = base.iloc[0:5000,14].values

from sklearn.preprocessing import LabelEncoder
labelencoder_previsores = LabelEncoder()
index = [1,3,5,6,7,8,9,13]
for n in index:
	previsores[:,n] = labelencoder_previsores.fit_transform(previsores[:,n])

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

from sklearn.model_selection import train_test_split
previsores_train, previsores_test, classe_train, classe_test = train_test_split(previsores, classe, test_size=0.15, random_state=0)

from sklearn.decomposition import KernelPCA
pca = KernelPCA(n_components = 6, kernel = 'rbf')
previsores_train = pca.fit_transform(previsores_train)
previsores_test = pca.fit_transform(previsores_test)

from sklearn.ensemble import RandomForestClassifier
classificador = RandomForestClassifier(n_estimators = 40, criterion = 'entropy', random_state = 0)
classificador.fit(previsores_train, classe_train)
previsoes = classificador.predict(previsores_test)

from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao)
print(matriz)

