import pandas as pd
base = pd.read_csv('../../NaiveBayes/Census/census.csv')
previsores = base.iloc[:,0:14].values
classe = base.iloc[:,14].values

from sklearn.preprocessing import LabelEncoder
labelencoder_previsores = LabelEncoder()
index = [1,3,5,6,7,8,9,13]
for n in index:
	previsores[:,n] = labelencoder_previsores.fit_transform(previsores[:,n])

#from sklearn.preprocessing import OneHotEncoder
#onehotencoder = OneHotEncoder(categorical_features=index)
#previsores = onehotencoder.fit_transform(previsores).toarray()

#labelencoder_classe = LabelEncoder()
#classe = labelencoder_classe.fit_transform(classe)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

from sklearn.model_selection import train_test_split
previsores_train, previsores_test, classe_train, classe_test = train_test_split(previsores, classe, test_size=0.15, random_state=0)

from sklearn.decomposition import PCA
pca = PCA(n_components = 10)
previsores_train = pca.fit_transform(previsores_train)
previsores_test = pca.fit_transform(previsores_test)

#pca = PCA(n_components = None)
#print(pca.explained_variance_ratio_)
# [0.15085409 0.10014287 0.09079838 0.0795386  0.07723336 0.0734284
# 0.06936772 0.06384516 0.06268856 0.05966572 0.05344505 0.04903029
# 0.04297258 0.02698922]

from sklearn.ensemble import RandomForestClassifier
classificador = RandomForestClassifier(n_estimators = 40, criterion = 'entropy', random_state = 0)
classificador.fit(previsores_train, classe_train)
previsoes = classificador.predict(previsores_test)

from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao)
print(matriz)
