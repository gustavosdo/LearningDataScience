# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
import numpy as np
import pickle
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler

folder = '/home/gustavo/Dropbox/Git/LearningDataScience/udemy/MLeDS/AlgorithmsEvaluation/CreditData/'
svm = pickle.load(open(folder+'classificador_svm.sav', 'rb'))
rf = pickle.load(open(folder+'classificador_rf.sav', 'rb'))
NN = pickle.load(open(folder+'classificador_NN.sav', 'rb'))

scaler = StandardScaler()
new = np.asarray([[50000, 40, 5000]])
new = new.reshape(-1,1)
new = scaler.fit_transform(new)
new = new.reshape(-1,3)

answer_svm = svm.predict(new)
answer_rf = rf.predict(new)
answer_NN = NN.predict(new)

answers = [answer_svm, answer_rf, answer_NN]

n_1 = 0
n_0 = 0

for ans in answers:
	if ans[0] == 1:
		n_1 += 1
	else:
		n_0 += 1

if n_1 > n_0: print('Cliente paga o empréstimo')
elif n_1 == n_0: print('Empate')
else: print('Cliente não pagará o empréstimo')
