import Orange

data = Orange.data.Table('credit-data.csv')
print(data.domain)

data_divided = Orange.evaluation.testing.sample(data, n=0.25)
data_training = data_divided[1]
data_testing = data_divided[0]

print(len(data_training))
print(len(data_testing))

classificator = Orange.classification.MajorityLearner()
result = Orange.evaluation.testing.TestOnTestData(data_training, data_testing, [classificator])

print(Orange.evaluation.CA(result))

from collections import Counter
print(Counter(str(d.get_class()) for d in data_testing))

### Também chamado de zeroR ou base line classifier,
### a ideia do majority learner é apenas classificar
### novas entradas como a maioria das entradas na base
### original. Repare que não há regras sendo aplicadas
### mas apenas uma contagem. A eficiência deste classificador
### então é simplesmente o número de contagens da maioria dividida pelo
### total de entradas. Portanto, nenhum método que tenha acurácia
### menor ou igual a esta eficiência deve ser levado em conta
### já que o custo computacional seria muito alto pra atingir o mesmo patamar
### de acurácia.
