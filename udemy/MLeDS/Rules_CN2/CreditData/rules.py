import Orange

base = Orange.data.Table('credit-data.csv')
print(base.domain)

# test and train samples
base_dividida = Orange.evaluation.testing.sample(base, n=0.25)
base_treinamento = base_dividida[1]
base_teste = base_dividida[0]
print(len(base_treinamento))
print(len(base_teste))

cn2_learner = Orange.classification.rules.CN2Learner()
classificador = cn2_learner(base_treinamento)

for regras in classificador.rule_list:
	print(regras)

# testing on test sample
# getting accuracy
resultado = Orange.evaluation.testing.TestOnTestData(base_treinamento, base_teste, [classificador])
#print(Orange.evaluation.CA(resultado)) # do not work!
