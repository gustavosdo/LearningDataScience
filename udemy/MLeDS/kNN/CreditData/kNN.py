import pandas as pd
dataset = pd.read_csv('credit-data.csv')

age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

previewPars = dataset.iloc[:, 1:4].values # loc with index
targetPar = dataset.iloc[:, 4].values

import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(previewPars[:,0:3]) # define the place to input
previewPars[:,0:3] = imputer.transform(previewPars[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previewPars = scaler.fit_transform(previewPars)

from sklearn.model_selection import train_test_split
previewPars_train, previewPars_test, targetPartrain, targetPartest = train_test_split(previewPars, targetPar, test_size=0.25, random_state=0)

from sklearn.neighbors import KNeighborsClassifier
classificator = KNeighborsClassifier(n_neighbors=5, metric='minkowski', p=2)
classificator.fit(previewPars_train, targetPartrain)
previsoes = classificator.predict(previewPars_test)

from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(targetPartest,previsoes)
matriz = confusion_matrix(targetPartest,previsoes)
print(precisao)
print(matriz)

import collections
collections.Counter(targetPartest)
