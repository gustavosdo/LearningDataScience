from math import *
from random import randint

def getVectors():
	x = [randint(-10,10), randint(-10,10), randint(-10,10)]
	y = [randint(-10,10), randint(-10,10), randint(-10,10)]
	print(x,y)
	return(x,y)

def EuclidianDistance():
	x,y = getVectors()
	ED2 = 0
	for i in range(0,len(x)):
		a = x[i]
		b = y[i]
		ED2 = (a-b)**2. + ED2
	ED = sqrt(ED2)
	print(ED)

EuclidianDistance()
