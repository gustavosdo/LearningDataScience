import pandas as pd
# uploading dataset
base = pd.read_csv('census.csv')
# getting the previsores variables
previsores = base.iloc[:,0:14].values
# class variable
classe = base.iloc[:,14].values

# labelling the nominals variables
from sklearn.preprocessing import LabelEncoder
labelencoder_previsores = LabelEncoder()
index = [1,3,5,6,7,8,9,13]
for n in index:
	previsores[:,n] = labelencoder_previsores.fit_transform(previsores[:,n])

# transform the race variable to each specific race variable based (dummy variables)
#from sklearn.preprocessing import OneHotEncoder
#onehotencoder = OneHotEncoder(categorical_features=index)
#previsores = onehotencoder.fit_transform(previsores).toarray()

labelencoder_classe = LabelEncoder()
classe = labelencoder_classe.fit_transform(classe)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

# spliting test and train samples
from sklearn.model_selection import train_test_split
previsores_train, previsores_test, classe_train, classe_test = train_test_split(previsores, classe, test_size=0.15, random_state=0)

from sklearn.ensemble import RandomForestClassifier
classificador = RandomForestClassifier(n_estimators = 40, criterion = 'entropy', random_state = 0)
classificador.fit(previsores_train, classe_train)
previsoes = classificador.predict(previsores_test)

# metricas de eficiencia
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao,matriz)

