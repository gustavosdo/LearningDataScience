import pandas as pd
# uploading dataset
base = pd.read_csv('census.csv')
# getting the previsores variables
previsores = base.iloc[:,0:14].values
#print(previsores[0:3])
# class variable
classe = base.iloc[:,14].values
#print(classe[0:3])

# labelling the nominals variables
from sklearn.preprocessing import LabelEncoder
labelencoder_previsores = LabelEncoder()
#labels = labelencoder_previsores.fit_transform(previsores[:,1])
#print(labels[0:10])
index = [1,3,5,6,7,8,9,13]
for n in index:
	previsores[:,n] = labelencoder_previsores.fit_transform(previsores[:,n])

#print(previsores[0:3])

# transform the race variable to each specific race variable based (dummy variables)
from sklearn.preprocessing import OneHotEncoder
#onehotencoder = OneHotEncoder(categories='auto',categorical_features=[0])
onehotencoder = OneHotEncoder(categorical_features=index)
previsores = onehotencoder.fit_transform(previsores).toarray()
#print(previsores[0:5])

labelencoder_classe = LabelEncoder()
classe = labelencoder_classe.fit_transform(classe)
#print(classe[0:5])

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

# spliting test and train samples
from sklearn.model_selection import train_test_split
previsores_train, previsores_test, classe_train, classe_test = train_test_split(previsores, classe, test_size=0.15, random_state=0)
'''
# Naive-bayes
from sklearn.naive_bayes import GaussianNB
classificador = GaussianNB()
classificador.fit(previsores_train, classe_train)
previsoes = classificador.predict(previsores_test)

# metricas de eficiencia
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao)
print(matriz)
'''

from sklearn.tree import DecisionTreeClassifier
classificador = DecisionTreeClassifier(criterion = 'entropy', random_state = 0)
classificador.fit(previsores_train, classe_train)
previsoes = classificador.predict(previsores_test)

# metricas de eficiencia
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao,matriz)

