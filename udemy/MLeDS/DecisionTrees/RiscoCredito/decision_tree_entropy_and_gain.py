# the input of this function is any list
# with repeated items. the output is a list
# with all the available values in the input
# but not repeated
def singleValues(someList):
	uniqueValuesList = []
	for item in someList:
		if (item in uniqueValuesList): pass
		else: uniqueValuesList.append(item)
	return (uniqueValuesList)

# lendo os dados
import pandas as pd
dataset = pd.read_csv('risco-credito.csv')

# Entropy = soma (-pi log2 pi)
# Gain = Entropy - soma(Sv/S Entropy)

# calculo da entropia
riscos = list(dataset['risco'].values) # o risco de cada item
n_alto = riscos.count('alto') # numero de riscos altos e assim por diante...
n_moderado = riscos.count('moderado')
n_baixo = riscos.count('baixo')

N = n_alto + n_moderado + n_baixo # numero total de itens

# valores p_i
p_alto = 1.*n_alto/N
p_moderado = 1.*n_moderado/N
p_baixo = 1.*n_baixo/N
p_i = [p_baixo, p_moderado, p_alto]

# entropia para cada item e entropia total
from math import log2
from functools import reduce
entropia_i = map(lambda a: -a*log2(a), p_i)
entropia = reduce(lambda x,y: x+y, entropia_i)

# Ganho de informacao para cada previsor
'''# considerando historia de credito como raiz da arvore
historia_i = list(dataset['historia'].values)'''
# considerando divida como raiz da arvore
historia_i = list(dataset['renda'].values)
total = len(historia_i)
historia_valores = singleValues(historia_i) # valores possiveis para a historia
historia_probabilidades = []
for valor in historia_valores:
	temp = historia_i.count(valor)
	historia_probabilidades.append([valor,1.*temp/total])
#print(historia_probabilidades)

entropias = []
for item in historia_probabilidades:
	#risco_i = list(dataset['risco'][dataset.historia == item[0]].values)
	risco_i = list(dataset['risco'][dataset.renda == item[0]].values)
	total = len(risco_i)
	risco_valores = singleValues(risco_i)
	risco_probabilidades = []
	risco_valores_probs = []
	for valor in risco_valores:
		temp = risco_i.count(valor)
		risco_probabilidades.append(1.*temp/total)
		risco_valores_probs.append([valor,1.*temp/total])
	entropias_i = map(lambda a: -a*log2(a), risco_probabilidades)
	entropias.append( [ item[0], reduce(lambda x,y: x+y, entropias_i) ] )

ganho = entropia
for prob in historia_probabilidades:
	for entropy in entropias:
		if (prob[0] == entropy[0]): ganho -= prob[1]*entropy[1]


print(ganho)
