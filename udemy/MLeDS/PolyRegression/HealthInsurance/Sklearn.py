# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
import numpy as np
#dataset = pd.read_csv('../LinearRegresion/HealthInsurance/plano-saude.csv')
dataset = pd.read_csv('plano-saude2.csv',dtype=float)

ages   = dataset.iloc[:,0:1].values # a
values = dataset.iloc[:,1].values # b

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(ages,values)

b = regressor.intercept_
a = regressor.coef_
print(regressor.score(ages,values))
print(a*40 + b)

from matplotlib import pyplot as plt
plt.scatter(ages,values)
plt.title('Regressão linear simples')
plt.plot(ages,regressor.predict(ages), color='red')
plt.xlabel('Idade')
plt.ylabel('Custo')
plt.show()
plt.clf()
plt.cla()
plt.close()

from sklearn.preprocessing import PolynomialFeatures
polyReg = PolynomialFeatures(degree = 4)
ages_poly = polyReg.fit_transform(ages)

regressor2 = LinearRegression()
regressor2.fit(ages_poly, values)
print(regressor2.score(ages_poly, values))
print(regressor2.predict(polyReg.fit_transform([[40]])))

plt.scatter(ages,values)
plt.title('Regressão polinomial')
plt.plot(ages,regressor2.predict(polyReg.fit_transform(ages)), color='red')
plt.xlabel('Idade')
plt.ylabel('Custo')
plt.show()
plt.clf()
plt.cla()
plt.close()

#from yellowbrick.regressor import ResidualsPlot
#view = ResidualsPlot(regressor)
#view.fit(ages,values)
#view.poof()

