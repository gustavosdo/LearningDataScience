import pandas as pd

data = pd.read_csv('~/Dropbox/Git/LearningDataScience/udemy/MLeDS/LinearRegresion/HousePrices/house-prices.csv')

sizes = data.iloc[:,3:19].values
prices = data.iloc[:,2].values

from sklearn.model_selection import train_test_split
sizes_train, sizes_test, prices_train, prices_test = train_test_split(sizes,prices,test_size=0.3,random_state=0)

from sklearn.preprocessing import PolynomialFeatures
poly = PolynomialFeatures(degree = 4)
sizes_train_poly = poly.fit_transform(sizes_train) # for each parameter create more 3 (potencies)
sizes_test_poly = poly.transform(sizes_test)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(sizes_train_poly, prices_train)

print( regressor.score(sizes_train_poly, prices_train) )

previsoes = regressor.predict(sizes_test_poly)

from sklearn.metrics import mean_absolute_error
print( mean_absolute_error(prices_test, previsoes) )
