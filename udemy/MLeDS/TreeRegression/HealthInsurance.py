# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
import numpy as np
#dataset = pd.read_csv('../LinearRegresion/HealthInsurance/plano-saude.csv')
dataset = pd.read_csv('/home/gustavo/Dropbox/Git/LearningDataScience/udemy/MLeDS/PolyRegression/HealthInsurance/plano-saude2.csv',dtype=float)

ages   = dataset.iloc[:,0:1].values # a
values = dataset.iloc[:,1].values # b

from sklearn.tree import DecisionTreeRegressor
regressor = DecisionTreeRegressor()
regressor.fit(ages, values)
print(regressor.score(ages, values))
#print(regressor2.predict(regressor.fit_transform([[40]])))
print(regressor.predict([[40]]))

import matplotlib.pyplot as plt
#plt.scatter(ages,values)
#plt.title('Regressão decision tree')
#plt.plot(ages,regressor.predict(ages), color='red')
#plt.xlabel('Idade')
#plt.ylabel('Custo')
#plt.show()
#plt.clf()
#plt.cla()
#plt.close()

ages_test = np.arange(min(ages),max(ages),0.1)
ages_test = ages_test.reshape(-1,1)
plt.scatter(ages,values)
plt.title('Regressão decision tree')
plt.plot(ages_test,regressor.predict(ages_test), color='red')
plt.xlabel('Idade')
plt.ylabel('Custo')
plt.show()
plt.clf()
plt.cla()
plt.close()
