import pandas as pd

data = pd.read_csv('~/Dropbox/Git/LearningDataScience/udemy/MLeDS/LinearRegresion/HousePrices/house-prices.csv')

sizes = data.iloc[:,3:19].values
prices = data.iloc[:,2].values

from sklearn.model_selection import train_test_split
sizes_train, sizes_test, prices_train, prices_test = train_test_split(sizes,prices,test_size=0.3,random_state=0)

from sklearn.tree import DecisionTreeRegressor
treeRegressor = DecisionTreeRegressor()
treeRegressor.fit(sizes_train, prices_train)

print( treeRegressor.score(sizes_train, prices_train) )

previsoes = treeRegressor.predict(sizes_test)

from sklearn.metrics import mean_absolute_error
print( treeRegressor.score(sizes_test, prices_test) )
print( mean_absolute_error(prices_test, previsoes) )
