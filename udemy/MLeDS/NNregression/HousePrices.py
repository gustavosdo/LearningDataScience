import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split

data = pd.read_csv('~/Dropbox/Git/LearningDataScience/udemy/MLeDS/LinearRegresion/HousePrices/house-prices.csv')

sizes = data.iloc[:,3:19].values
prices = data.iloc[:,2:3].values

scaler_x = StandardScaler()
scaler_y = StandardScaler()
x = scaler_x.fit_transform(sizes)
y = scaler_y.fit_transform(prices)

x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.3,random_state=0)

#regressor = MLPRegressor(hidden_layer_sizes = (9,9)) # 2 camadas ocultas com 9 neuronios cada: #neuronios ~ (#saida + #entrada)/2
regressor = MLPRegressor(hidden_layer_sizes = (9,9,9,9)) # 4 camadas ocultas com 9 neuronios cada: #neuronios ~ (#saida + #entrada)/2
regressor.fit(x_train, y_train)

print(regressor.score(x_train, y_train))

print( regressor.score(x_test, y_test) )

previsoes = regressor.predict(x_test)
y_test = scaler_y.inverse_transform(y_test)
previsoes = scaler_y.inverse_transform(previsoes)
print( mean_absolute_error(y_test, previsoes) )
