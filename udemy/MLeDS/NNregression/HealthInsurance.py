# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPRegressor

dataset = pd.read_csv('/home/gustavo/Dropbox/Git/LearningDataScience/udemy/MLeDS/PolyRegression/HealthInsurance/plano-saude2.csv',dtype=float)

ages   = dataset.iloc[:,0:1].values # a
values = dataset.iloc[:,1:2].values # b

scaler_ages = StandardScaler()
ages = scaler_ages.fit_transform(ages)
scaler_values = StandardScaler()
values = scaler_values.fit_transform(values)

regressor = MLPRegressor()
regressor.fit(ages, values)
print(regressor.score(ages, values))
print( scaler_values.inverse_transform(regressor.predict(scaler_ages.transform([[40]]))) )

ages_test = np.arange(min(ages),max(ages),0.1)
ages_test = ages_test.reshape(-1,1)
plt.scatter(ages,values)
plt.title('Regressão ANN')
plt.plot(ages_test,regressor.predict(ages_test), color='red')
plt.xlabel('Idade')
plt.ylabel('Custo')
plt.show()
plt.clf()
plt.cla()
plt.close()
