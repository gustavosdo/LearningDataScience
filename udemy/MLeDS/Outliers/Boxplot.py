# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('../NaiveBayes/CreditData/credit-data.csv')
dataset = dataset.dropna()

## outliers age
import matplotlib.pyplot as plt
plt.boxplot(dataset.iloc[:,2], showfliers = True) # False to not show outliers
print(dataset[(dataset.age < -20)])
plt.show()

## outliers loan
plt.boxplot(dataset.iloc[:,3])
print(dataset[(dataset.loan > 13400)])
plt.show()
