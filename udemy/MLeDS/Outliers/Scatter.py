# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('../NaiveBayes/CreditData/credit-data.csv')
dataset = dataset.dropna()

## Changing the negative values of ages by the mean age
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

## income vs age
import matplotlib.pyplot as plt
plt.scatter(dataset.iloc[:,1], dataset.iloc[:,2])
plt.show()

## income vs loan
import matplotlib.pyplot as plt
plt.scatter(dataset.iloc[:,1], dataset.iloc[:,3])
plt.show()

## age vs loan
import matplotlib.pyplot as plt
plt.scatter(dataset.iloc[:,2], dataset.iloc[:,3])
plt.show()

## New data (census)
census = pd.read_csv('../NaiveBayes/Census/census.csv')

plt.scatter(census.iloc[:,0], census.iloc[:,2])
plt.show()
