# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('../NaiveBayes/CreditData/credit-data.csv')
dataset = dataset.dropna()

from pyod.models.knn import KNN
detector = KNN()
detector.fit(dataset.iloc[:,1:4])

#print(detector.labels_)
#print(detector.decision_scores_)

outliers = []
for i in range(len(detector.labels_)):
	if detector.labels_[i] == 1:
		outliers.append(i)

lista_outliers = dataset.iloc[outliers, :]
print(lista_outliers)
