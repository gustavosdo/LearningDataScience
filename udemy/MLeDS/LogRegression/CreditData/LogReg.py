# -*- coding: utf-8 -*-
# reading data
import pandas as pd
dataset = pd.read_csv('credit-data.csv')

# non-negative ages mean -> change the negative ones by this mean
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

# splitting previewers and class variables
previewers = dataset.iloc[:, 1:4].values # loc with index
target = dataset.iloc[:, 4].values

import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(previewers[:,0:3]) # define the place to input
previewers[:,0:3] = imputer.transform(previewers[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previewers = scaler.fit_transform(previewers)

# split train and test samples
from sklearn.model_selection import train_test_split
previewers_train, previewers_test, targettrain, targettest = train_test_split(previewers, target, test_size=0.25, random_state=0)

from sklearn.linear_model import LogisticRegression
classificator = LogisticRegression(solver='liblinear', random_state=1)
classificator.fit(previewers_train, targettrain)
previsoes = classificator.predict(previewers_test)

from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(targettest,previsoes)
matriz = confusion_matrix(targettest,previsoes)
print(precisao)
print(matriz)
