# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
dataset = pd.read_csv('plano-saude.csv')

ages   = dataset.iloc[:,0].values # a
values = dataset.iloc[:,1].values # b

import numpy as np
correlacao = np.corrcoef(ages,values)

ages = ages.reshape(-1,1)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(ages,values)

b = regressor.intercept_
a = regressor.coef_
print(b)
print(a)

import matplotlib.pyplot as plt
plt.scatter(ages,values)
plt.title('Regressão linear simples')
plt.plot(ages,regressor.predict(ages))
plt.xlabel('Idade')
plt.ylabel('Custo')
plt.show()

# prediction
print(a*30 + b)

score = regressor.score(ages,values)
print(score)

from yellowbrick.regressor import ResidualsPlot
view = ResidualsPlot(regressor)
view.fit(ages,values)
view.poof()

