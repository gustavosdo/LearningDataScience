# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
dataset = pd.read_csv('plano-saude.csv')

ages   = list(dataset.iloc[:,0].values) # a
values = list(dataset.iloc[:,1].values) # b

from statistics import mean as M
from math import sqrt

mean_age = M(ages)
square_mean_age = mean_age*mean_age
square_ages = [x*x for x in ages]
mean_square_age = M(square_ages)

mean_value = M(values)

ages_values = [x*y for x,y in zip(ages,values)]

mean_age_value = M(ages_values)

c_1 = (mean_age_value - mean_age*mean_value)/(mean_square_age - square_mean_age)
c_0 = mean_value - c_1*mean_age
zero_value_age = -c_0/c_1
value_age_30 = c_0 + c_1*30

print('The function obtained by linear regression is: value = %.2f + %.2f . age' %(c_0,c_1))
print('Age for value = 0: %.2f' %zero_value_age)
print('Value for age = 30: %.2f' %value_age_30)

# Pearson correlation
covariance = sum([ (x - mean_age)*(y - mean_value) for x,y in zip(ages,values)])
std_age = sqrt( sum( [ (x - mean_age)*(x - mean_age) for x in ages] ) )
std_value = sqrt( sum( [ (x - mean_value)*(x - mean_value) for x in values] ) )

r = covariance/(std_age * std_value)

print('Pearson correlation = %.6f' %r)
