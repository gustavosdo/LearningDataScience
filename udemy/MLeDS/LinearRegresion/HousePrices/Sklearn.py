import pandas as pd

data = pd.read_csv('house-prices.csv')

sizes = data.iloc[:,5:6].values # in order to have a column in the data (not necessary to use the reshape)
prices = data.iloc[:,2].values

from sklearn.model_selection import train_test_split
sizes_train, sizes_test, prices_train, prices_test = train_test_split(sizes,prices,test_size=0.3,random_state=0)

from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(sizes_train, prices_train)
score = regressor.score(sizes_test, prices_test)
print(score)

import matplotlib.pyplot as plt
plt.scatter(sizes_train, prices_train)
plt.plot(sizes_train, regressor.predict(sizes_train), color='red')
plt.show()

# scores
previsoes = regressor.predict(sizes_test)
print(previsoes)
resultado = abs(prices_test - previsoes)
print(resultado)
print(resultado.mean())

# with sklearn
from sklearn.metrics import mean_absolute_error, mean_squared_error
mae = mean_absolute_error(prices_test, previsoes)
mse = mean_squared_error(prices_test, previsoes)
print(mse,mae)

plt.scatter(sizes_test, prices_test)
plt.plot(sizes_test, regressor.predict(sizes_test), color='red')
plt.show()
