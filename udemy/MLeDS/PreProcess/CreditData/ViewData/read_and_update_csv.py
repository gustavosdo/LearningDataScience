# First code to read the credit database
# Generates two plots:
# A distribution of age (histogram)
# Income (y) vs Age (x)

# Import pandas to read and math to use isnan function
import pandas
import math

# Simply reading the csv file (some common options are available in the commented lines)
#df = pandas.read_csv('../dataset/credit-data.csv', delimiter=",", encoding='utf-8')
#df = pandas.read_csv('../dataset/credit-data.csv', sep='::', engine='python')
df = pandas.read_csv('../dataset/credit-data.csv')

# First crude data visualization
print(df.describe())
print(df[0:2])

# Looking for inconsistent age values
for age in df.loc[:,'age'].values:
	if(age <= 0 or math.isnan(age)) : print(age)

nan_index = []
for row in df.loc[:,'clientid':'age'].values:
	#print(row)
	#if math.isnan(row[2]): print(row[0],row[2])
	if math.isnan(row[2]): nan_index.append(int(row[0]-1)) # gets the rows with NaN ages
	if (row[2] < 0): df.at[row[0]-1,'age'] = -row[2]

#print(df)
#print(nan_index)
df_update = df.drop(df.index[nan_index])
#print(df_update)
export_csv = df_update.to_csv (r'../dataset/credit-data-update.csv', header=True)


##########################
# Select those who paid full loan

unpaid_index = []
paid_index = []
for row in df_update.loc[:,'clientid':'default'].values:
	if(row[4] == 0): unpaid_index.append(int(row[0]-1))
	if(row[4] == 1): paid_index.append(int(row[0]-1))

#print(unpaid_index) # len = 1714
#print(paid_index) # len = 283

#df_paid = df_update.drop(df_update.index[unpaid_index])
#print(df_paid)
#export_csv = df_paid.to_csv (r'../dataset/credit-data-paid.csv', header=True)

