# Generates two plots:
# A distribution of age (histogram)
# Income and loan (y) vs Age (x)

# Import pandas to read and matplotlib to make plots
import pandas
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['font.sans-serif'] = ['Amiri', 'sans-serif']
matplotlib.rcParams['font.size'] = 14

# Simply reading the csv file 
df = pandas.read_csv('../dataset/credit-data-update.csv')

# getting all ages
ages = df.loc[:,'age'].values

#print(ages)
#print(len(ages))

# Prepare plot
plt.hist(ages,
         bins=50,
         density=False,
         histtype='bar',
         color='b',
         edgecolor='k',
         alpha=0.5)

# labels and ticks
plt.xlabel('Age (years)')
plt.xticks([20,30,40,50,60])
plt.ylabel('Number of persons')
plt.title('Histogram of persons divided by age')

# show plot
plt.savefig('ages_histogram.jpg', dpi=300, quality=95)
#plt.show()

plt.clf()

# getting all incomes and loans
incomes = df.loc[:,'income'].values
loans = df.loc[:,'loan'].values

plt.scatter(ages, incomes, color='b')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel('Age (years)')
plt.ylabel('Income ($)')
plt.title('Income vs Age')

plt.savefig('income_by_age.jpg', dpi=300, quality=95)

plt.clf()

plt.scatter(ages, loans, color='r')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel('Age (years)')
plt.ylabel('Loan ($)')
plt.title('Loan vs Age')

plt.savefig('loan_by_age.jpg', dpi=300, quality=95)

plt.clf()

plt.scatter(incomes, loans, color='g')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.xlabel('Income ($)')
plt.ylabel('Loan ($)')
plt.title('Loan vs Income')

plt.savefig('loan_by_income.jpg', dpi=300, quality=95)
