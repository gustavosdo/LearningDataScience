import pandas as pd

data = pd.read_csv('~/Dropbox/Git/LearningDataScience/udemy/MLeDS/LinearRegresion/HousePrices/house-prices.csv')

sizes = data.iloc[:,3:19].values
prices = data.iloc[:,2:3].values

from sklearn.preprocessing import StandardScaler
scaler_x = StandardScaler()
scaler_y = StandardScaler()
x = scaler_x.fit_transform(sizes)
y = scaler_y.fit_transform(prices)

from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.3,random_state=0)

from sklearn.svm import SVR
Regressor = SVR(kernel='rbf')
Regressor.fit(x_train, y_train)

print( Regressor.score(x_train, y_train) )

print( Regressor.score(x_test, y_test) )

from sklearn.metrics import mean_absolute_error
previsoes = Regressor.predict(x_test)
y_test = scaler_y.inverse_transform(y_test)
previsoes = scaler_y.inverse_transform(previsoes)
print( mean_absolute_error(y_test, previsoes) )
