import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import StandardScaler

# data
ages = [20, 27, 21, 37, 46, 53, 55, 47, 52, 32, 39, 41, 39, 48, 48]
wages = [1000, 1200, 2900, 1850, 920, 950, 2000, 2100, 3000, 5900, 4100, 5100, 7000, 5000, 6500]

# plot data
'''plt.scatter(ages,wages)
plt.show()'''

# dataset ready to be analyzed
#dataset = np.array( [ [ages[i],wages[i]] for i in range(0,len(ages)) ] )
dataset = np.array(list(zip(ages,wages)))
scaler = StandardScaler()
dataset = scaler.fit_transform(dataset) # if the scaler is not applied a greater importance is given to the difference in the wages
#print(dataset)

# building dendrogram with ward method
dg = hierarchy.linkage(dataset, method='ward')

# plotting dendogram in order to choose n_clusters
hierarchy.dendrogram(dg)
plt.ylabel('Euclidian distance')
plt.xlabel('Clients')
plt.show()

# Apply hierarchy algorithm
hc = AgglomerativeClustering(n_clusters=3, affinity = 'euclidean', linkage = 'ward')
predictions = hc.fit_predict(dataset)

# plot clusters
colors = ['green', 'red', 'blue']
for i in range(0, len(colors)):
	plt.scatter(dataset[predictions == i, 0], dataset[predictions == i, 1], c = colors[i], s = 100, label = 'Cluster '+str(i))
#plt.scatter(centroids[:,0], centroids[:,1], marker = 'x')
plt.xlabel('Ages')
plt.ylabel('Wages')
plt.legend()
plt.show()
