import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering

dataset = pd.read_csv('../K-means/credit-card-clients.csv', header = 1) # header on second line
# defining a new parameter by the sum of 6 original attributes
dataset['BILL_TOTAL'] = dataset['BILL_AMT1'] +\
						dataset['BILL_AMT2'] +\
						dataset['BILL_AMT3'] +\
						dataset['BILL_AMT4'] +\
						dataset['BILL_AMT5'] +\
						dataset['BILL_AMT6']

# parameters (dimensions) in which the clustering is performed
#X = dataset.iloc[:, [1,25]].values # MemoryError: unable to allocate array data
X = dataset.iloc[0:100, [1,25]].values
scaler = StandardScaler() # scaling the attributes
X = scaler.fit_transform(X)

# building dendrogram with ward method
dg = hierarchy.linkage(X, method='ward')

# plotting dendogram in order to choose n_clusters
hierarchy.dendrogram(dg)
plt.ylabel('Euclidian distance')
plt.xlabel('Clients')
plt.show()


# Apply hierarchy algorithm
hc = AgglomerativeClustering(n_clusters=3, affinity = 'euclidean', linkage = 'ward')
predictions = hc.fit_predict(X)

# plot clusters
colors = ['green', 'red', 'blue']
for i in range(0, len(colors)):
	plt.scatter(X[predictions == i, 0], X[predictions == i, 1], c = colors[i], s = 100, label = 'Cluster '+str(i))
#plt.scatter(centroids[:,0], centroids[:,1], marker = 'x')
plt.xlabel('Limits')
plt.ylabel('Bill total amount')
plt.legend()
plt.show()
