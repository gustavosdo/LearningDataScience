import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.cluster import DBSCAN
from scipy.cluster import hierarchy
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering
from sklearn import datasets

x,y = datasets.make_moons(n_samples = 1500,noise=0.09)
plt.scatter(x[:,0], x[:,1], s=5)
plt.savefig('moons.png')

colors = np.array(['red', 'blue'])

kmeans = KMeans(n_clusters = 2)
predictions = kmeans.fit_predict(x)
plt.scatter(x[:,0], x[:,1], s=5, color = colors[predictions])
plt.savefig('kmeans_moons.png')

hc = AgglomerativeClustering(n_clusters = 2, affinity = 'euclidean', linkage = 'ward')
predictions = hc.fit_predict(x)
plt.scatter(x[:,0], x[:,1], s=5, color = colors[predictions])
plt.savefig('hierarchy_moons.png')

dbscan = DBSCAN(eps = 0.1)
predictions = dbscan.fit_predict(x)
plt.scatter(x[:,0], x[:,1], s=5, color = colors[predictions])
plt.savefig('dbscan_moons.png')
