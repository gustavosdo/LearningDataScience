import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.cluster import DBSCAN

dataset = pd.read_csv('../K-means/credit-card-clients.csv', header = 1) # header on second line
# defining a new parameter by the sum of 6 original attributes
dataset['BILL_TOTAL'] = dataset['BILL_AMT1'] +\
						dataset['BILL_AMT2'] +\
						dataset['BILL_AMT3'] +\
						dataset['BILL_AMT4'] +\
						dataset['BILL_AMT5'] +\
						dataset['BILL_AMT6']

#X = dataset.iloc[0:100, [1,25]].values
X = dataset.iloc[:, [1,25]].values
scaler = StandardScaler() # scaling the attributes
X = scaler.fit_transform(X)

# dbscan with threshold distance = 0.5 and at least 5 points inside each dense region (cluster)
dbscan = DBSCAN(eps = 0.37, min_samples = 4)
predictions = dbscan.fit_predict(X)
uniques, quantity = np.unique(predictions, return_counts=True)
print(uniques, quantity)

# plot clusters
colors = ['green', 'red', 'blue']
for i in range(0, len(colors)):
	plt.scatter(X[predictions == i, 0], X[predictions == i, 1], c = colors[i], s = 100, label = 'Cluster '+str(i))
#plt.scatter(centroids[:,0], centroids[:,1], marker = 'x')
plt.xlabel('Limits')
plt.ylabel('Bill total amount')
plt.legend()
plt.show()
