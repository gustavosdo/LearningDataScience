import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN

# data
ages = [20, 27, 21, 37, 46, 53, 55, 47, 52, 32, 39, 41, 39, 48, 48]
wages = [1000, 1200, 2900, 1850, 920, 950, 2000, 2100, 3000, 5900, 4100, 5100, 7000, 5000, 6500]

dataset = np.array(list(zip(ages,wages)))
scaler = StandardScaler()
dataset = scaler.fit_transform(dataset) # if the scaler is not applied a greater importance is given to the difference in the wages

# dbscan with threshold distance = 0.5 and at least 5 points inside each dense region (cluster)
dbscan = DBSCAN(eps = 0.5, min_samples = 5)
dbscan.fit(dataset)

print(dbscan.labels_)
# all points are noise

# new parameters
dbscan = DBSCAN(eps = 0.95, min_samples = 2)
dbscan.fit(dataset)

predictions = dbscan.labels_
print(predictions)

# plot clusters
colors = ['green', 'red', 'blue']
for i in range(0, len(colors)):
	plt.scatter(dataset[predictions == i, 0], dataset[predictions == i, 1], c = colors[i], s = 100, label = 'Cluster '+str(i))
#plt.scatter(centroids[:,0], centroids[:,1], marker = 'x')
plt.xlabel('Ages')
plt.ylabel('Wages')
plt.legend()
plt.show()

