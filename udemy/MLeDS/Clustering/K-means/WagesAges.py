import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler

# data
ages = [20, 27, 21, 37, 46, 53, 55, 47, 52, 32, 39, 41, 39, 48, 48]
wages = [1000, 1200, 2900, 1850, 920, 950, 2000, 2100, 3000, 5900, 4100, 5100, 7000, 5000, 6500]

# plot data
'''plt.scatter(ages,wages)
plt.show()'''

# dataset ready to be analyzed
#dataset = np.array( [ [ages[i],wages[i]] for i in range(0,len(ages)) ] )
dataset = np.array(list(zip(ages,wages)))
scaler = StandardScaler()
dataset = scaler.fit_transform(dataset) # if the scaler is not applied a greater importance is given to the difference in the wages
#print(dataset)

# k-means algorithm definition and training
kmeans = KMeans(n_clusters = 3)
kmeans.fit(dataset)

# Centroids and labels
centroids = kmeans.cluster_centers_
labels = kmeans.labels_
print(centroids)
print(labels)

# plot clusters
colors = ['g.', 'r.', 'b.']
for i in range(0, len(ages)):
	plt.plot(dataset[i][0], dataset[i][1], colors[labels[i]], markersize = 15)
plt.scatter(centroids[:,0], centroids[:,1], marker = 'x')
plt.show()
