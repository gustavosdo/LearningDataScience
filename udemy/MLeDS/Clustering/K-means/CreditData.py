import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.datasets.samples_generator import make_blobs
from sklearn.metrics import accuracy_score, confusion_matrix

dataset = pd.read_csv('credit-card-clients.csv', header = 1) # header on second line
# defining a new parameter by the sum of 6 original attributes
dataset['BILL_TOTAL'] = dataset['BILL_AMT1'] +\
						dataset['BILL_AMT2'] +\
						dataset['BILL_AMT3'] +\
						dataset['BILL_AMT4'] +\
						dataset['BILL_AMT5'] +\
						dataset['BILL_AMT6']

# parameters (dimensions) in which the clustering is performed
#X = dataset.iloc[:, [1,25]].values
X = dataset.iloc[:, [1,2,3,4,5,25]].values
scaler = StandardScaler() # scaling the attributes
X = scaler.fit_transform(X)

# Elbow method
wcss = []
for C in range(1,11):
	kmeans = KMeans(n_clusters = C, random_state = 0) # fix random state in order to get the same answer for every run
	kmeans.fit(X)
	wcss.append(kmeans.inertia_) # inertia is the distances sum
# WCSS plot
plt.plot(range(1,11), wcss)
plt.xlabel('Clusters')
plt.ylabel('WCSS')
plt.show()
# We can choose both 4 or 5 (Jones); I would choose 8

# kmeans full algorithm with 4 clusters
kmeans = KMeans(n_clusters = 4, random_state = 0)
predictions = kmeans.fit_predict(X)

# no possible plot in this case
# use the client list to see the clients sorted by its cluster

client_list = np.column_stack((dataset,predictions))
client_list = client_list[client_list[:,26].argsort()]
print(client_list)
