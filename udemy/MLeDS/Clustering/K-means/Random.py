import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.datasets.samples_generator import make_blobs
from sklearn.metrics import accuracy_score, confusion_matrix

# random data with 200 inouts and 4 centers (centroids)
X,Y = make_blobs(n_samples = 200, centers = 4)

# print lists of data and the its centers
#print(X)
#print(Y)

# plot data
'''plt.scatter(X[:,0], X[:,1])
plt.show()'''

# kmeans algorithm definition an traning
kmeans = KMeans(n_clusters = 4)
kmeans.fit(X)

# is the training being executed correctly?
predictions = kmeans.predict(X)
print(confusion_matrix(Y, predictions))
print(accuracy_score(Y, predictions))

# plot all data with colors matching the clusters
plt.scatter(X[:,0], X[:,1], c = predictions)
plt.show()
