# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
import numpy as np
import pickle
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
dataset = pd.read_csv('credit-data.csv')
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean
params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution
scaler = StandardScaler()
params = scaler.fit_transform(params)

svm = pickle.load(open('classificador_svm.sav', 'rb'))
rf = pickle.load(open('classificador_rf.sav', 'rb'))
NN = pickle.load(open('classificador_NN.sav', 'rb'))

result_svm = svm.score(params, target_)
result_rf = rf.score(params, target_)
result_NN = NN.score(params, target_)

print(result_svm, result_rf, result_NN)

# testing new register
new = np.asarray([[50000, 40, 5000]])
new = new.reshape(-1,1) # reshape to make the saceling
new = scaler.fit_transform(new)
new = new.reshape(-1,3)

# classifying new register
answer_svm = svm.predict(new)
answer_rf = rf.predict(new)
answer_NN = NN.predict(new)

print(answer_svm, answer_rf, answer_NN)

