# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
dataset = pd.read_csv('credit-data.csv')

age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
params = scaler.fit_transform(params)

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.naive_bayes import GaussianNB
classificador = GaussianNB()
kfold = StratifiedKFold(n_splits = 10, shuffle = True, random_state = 4)
resultados = []
matrizes = []

for i_train, i_test in kfold.split(params, np.zeros(shape=(params.shape[0],1))):
	#print('Indice treinamento: ', i_train, 'Indice teste: ', i_test)
	classificador = GaussianNB()
	classificador.fit(params[i_train], target_[i_train])
	previsoes = classificador.predict(params[i_test])
	acuracia = accuracy_score(target_[i_test], previsoes)
	resultados.append(acuracia)
	matrizes.append(confusion_matrix(target_[i_test], previsoes))

import statistics
print(resultados)
print(statistics.mean(resultados))

matriz_final = np.mean(matrizes, axis=0)
print(matriz_final)
