# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
dataset = pd.read_csv('credit-data.csv')

age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
params = scaler.fit_transform(params)

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier

classificator_svm = SVC(kernel='rbf', C = 2.0, probability = True)
classificator_svm.fit(params,target_)

classificator_rf = RandomForestClassifier(n_estimators = 40, criterion = 'entropy')
classificator_rf.fit(params, target_)

classificator_NN = MLPClassifier(verbose = True,
								 max_iter = 1000,
								 tol = 0.00001,
								 solver = 'adam',
								 hidden_layer_sizes = (100),
								 activation = 'relu',
								 batch_size = 200,
								 learning_rate_init = 0.001)
classificator_NN.fit(params,target_)

import pickle
pickle.dump(classificator_svm, open('classificador_svm.sav', 'wb'))
pickle.dump(classificator_rf, open('classificador_rf.sav', 'wb'))
pickle.dump(classificator_NN, open('classificador_NN.sav', 'wb'))
