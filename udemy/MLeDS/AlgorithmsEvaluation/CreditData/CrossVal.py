# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

import pandas as pd
dataset = pd.read_csv('credit-data.csv')

age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
params = scaler.fit_transform(params)

from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
classificador = GaussianNB()

resultados = cross_val_score(classificador, params, target_, cv = 10)

print(resultados)
print(resultados.mean())
print(resultados.std()) # valores altos de std deviation podem indicar overfitting
