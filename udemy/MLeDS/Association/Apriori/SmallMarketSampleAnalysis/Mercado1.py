import pandas as pd # library to read csv files
dataset = pd.read_csv('mercado.csv', header = None) # read csv file mercado.csv w/o header
dataset = dataset.astype(str) # convert all entries in the DataFrame to strings (faster way)

compras = dataset.values.tolist() # transform each line in a list; insert all line-lists in another list (compras)

from apyori import apriori # library for apriori association rules
#regrasAssocia = apriori(compras) # using the apriori library in order to generate the association rules
regrasAssocia = apriori(compras,
						min_support = 0.3,
						min_confidence = 0.8,
						min_lift = 2,
						min_length = 2)# defining min support, confidence, lift and length (min items in a rule)

listaRegras = list(regrasAssocia)
#print(listaRegras) # printing the rules (hard-to-read way)
#print([list(x) for x in listaRegras]) # better...

# printing rule by rule:
listaRegras = [list(x) for x in listaRegras]
for i in range(0,3): # end can be 38 at max (for apriori w/o min support etc defined)
	print([list(x) for x in listaRegras[i][2]])

