import pandas as pd # library to read csv files
dataset = pd.read_csv('mercado2.csv', header = None) # read csv file mercado.csv w/o header
dataset = dataset.astype(str) # convert all entries in the DataFrame to strings (faster way)

compras = dataset.values.tolist() # transform each line in a list; insert all line-lists in another list (compras)

from apyori import apriori # library for apriori association rules
#regrasAssocia = apriori(compras) # using the apriori library in order to generate the association rules
''' In order to find the correct support for a product that is sold n times a week (7 days)
we can determine the support value by n*7/N where N is the number of sales (lines) in the database'''
exSupport = 4*7./7501
regrasAssocia = apriori(compras,
						min_support = exSupport,
						min_confidence = 0.5,
						min_lift = 3,
						min_length = 2)# defining min support, confidence, lift and length (min items in a rule)

listaRegras = list(regrasAssocia)
#print(listaRegras) # printing the rules (hard-to-read way)
#print([list(x) for x in listaRegras]) # better...

# printing rule by rule:
listaRegras = [list(x) for x in listaRegras]
for i in range(0,5): # end can be 38 at max (for apriori w/o min support etc defined)
	print([list(x) for x in listaRegras[i][2]])
