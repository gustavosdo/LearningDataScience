from pybrain.structure import FeedForwardNetwork,LinearLayer,SigmoidLayer,BiasUnit,FullConnection

rede = FeedForwardNetwork() # rede neural com alimentação no sentido frontal
camadaEntrada = LinearLayer(2) # linear pois não há função de ativação e 2 pois serão 2 neurônios
camadaOculta = SigmoidLayer(3)
camadaSaida = SigmoidLayer(1)

bias1 = BiasUnit()
bias2 = BiasUnit()

modulos  = [camadaEntrada, camadaOculta, camadaSaida, bias1, bias2]
for modulo in modulos:
	rede.addModule(modulo)

entradaOculta = FullConnection(camadaEntrada, camadaOculta)  # Full pois a ligação é entre todos os neurônios
ocultaSaida = FullConnection(camadaOculta, camadaSaida)

biasOculta = FullConnection(bias1, camadaOculta)
biasSaida = FullConnection(bias2, camadaSaida)

rede.sortModules()

print(rede)
print(entradaOculta.params)
print(ocultaSaida.params)
print(biasOculta.params)
print(biasSaida.params)
