## Construção por um atalho
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from pybrain.structure.modules import SigmoidLayer

## EXEMPLO
'''rede = buildNetwork(2,3,1,outclass=SoftmaxLayer, hiddenclass = SigmoidLayer, bias = False)

# tipos das camadas
print(rede['in'])
print(rede['hidden0'])
print(rede['out'])
print(rede['bias'])'''

# Construção da rede
rede = buildNetwork(2,3,1)
# construção dos dados
data = SupervisedDataSet(2,1)
data.addSample((0,0), (0, ))
data.addSample((0,1), (1, ))
data.addSample((1,0), (1, ))
data.addSample((1,1), (0, ))

training = BackpropTrainer(rede, dataset = data, learningrate = 0.01, momentum = 0.06)

# épocas
for i in range(1,30000):
	erro = training.train()
	if i%1000 == 0:
		print("Erro = %s" %erro)

print(rede.activate([0,0]))
print(rede.activate([1,0]))
print(rede.activate([0,1]))
print(rede.activate([1,1]))
