# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('credit-data.csv')

# non-negative ages mean -> change the negative ones by this mean
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

# splitting params and class variables
params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
params = scaler.fit_transform(params)

# split train and test samples
from sklearn.model_selection import train_test_split
params_train, params_test, target_train, target_test = train_test_split(params, target_, test_size=0.25, random_state=0)

# Naive-bayes
from sklearn.neural_network import MLPClassifier # multi-layer perceptron
classificator = MLPClassifier(verbose=True,
							  max_iter = 1000,
							  tol = 0.00001,
							  solver = 'adam',
							  hidden_layer_sizes = (100),
							  activation = 'relu',
							  learning_rate = 'adaptive')
classificator.fit(params_train, target_train)
previsoes = classificator.predict(params_test)

# usando scikit-learn
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(target_test,previsoes)
matriz = confusion_matrix(target_test,previsoes)
print(precisao)
print(matriz)
