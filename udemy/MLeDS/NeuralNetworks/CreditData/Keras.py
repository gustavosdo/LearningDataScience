# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('credit-data.csv')

# non-negative ages mean -> change the negative ones by this mean
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

# splitting params and class variables
params = dataset.iloc[:, 1:4].values # loc with index
target_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(params[:,0:3]) # define the place to input
params[:,0:3] = imputer.transform(params[:,0:3]) # performing the subtitution

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
params = scaler.fit_transform(params)

# split train and test samples
from sklearn.model_selection import train_test_split
params_train, params_test, target_train, target_test = train_test_split(params, target_, test_size=0.25, random_state=0)

import keras
from keras.models import Sequential # feed foraward tpe
from keras.layers import Dense # all perceptrons connect with the n+1 layer perceptrons
classificator = Sequential()
classificator.add(Dense(units = 2, activation = 'relu', input_dim = 3))
# units = number of perceptrons in the hidden layer, input_dim = # of params in the input layer
classificator.add(Dense(units = 2, activation = 'relu'))
classificator.add(Dense(units = 1, activation = 'sigmoid')) # output layer, 1 perceptron, sigmoid activation
classificator.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

classificator.fit(params_train, target_train, batch_size = 10, nb_epoch = 100) # mandatory params
previsoes = classificator.predict(params_test)
previsoes = (previsoes > 0.5) # True or False

# usando scikit-learn
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(target_test,previsoes)
matriz = confusion_matrix(target_test,previsoes)
print(precisao)
print(matriz)
