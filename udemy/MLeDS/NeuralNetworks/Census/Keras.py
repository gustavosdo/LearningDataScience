import pandas as pd
base = pd.read_csv('census.csv')
previsores = base.iloc[:,0:14].values
classe = base.iloc[:,14].values

from sklearn.preprocessing import LabelEncoder
labelencoder_previsores = LabelEncoder()
index = [1,3,5,6,7,8,9,13]
for n in index:
	previsores[:,n] = labelencoder_previsores.fit_transform(previsores[:,n])


from sklearn.preprocessing import OneHotEncoder
onehotencoder = OneHotEncoder(categorical_features=index)
previsores = onehotencoder.fit_transform(previsores).toarray()

labelencoder_classe = LabelEncoder()
classe = labelencoder_classe.fit_transform(classe)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
previsores = scaler.fit_transform(previsores)

# spliting test and train samples
from sklearn.model_selection import train_test_split
previsores_train, previsores_test, classe_train, classe_test = train_test_split(previsores, classe, test_size=0.15, random_state=0)

import keras
from keras.models import Sequential
from keras.layers import Dense
classificador = Sequential()
classificador.add(Dense(units = 55, activation = 'relu', input_dim = 108))
classificador.add(Dense(units = 55, activation = 'relu'))
classificador.add(Dense(units = 1, activation = 'sigmoid'))
#classificador.add(Dense(units = 3, activation = 'softmax')) more possible outputs (categorical)
classificador.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

classificador.fit(previsores_train, classe_train, batch_size = 10, epochs = 100) # mandatory params
previsoes = classificador.predict(previsores_test)
previsoes = (previsoes > 0.5) # True or False

# usando scikit-learn
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(classe_test,previsoes)
matriz = confusion_matrix(classe_test,previsoes)
print(precisao)
print(matriz)
