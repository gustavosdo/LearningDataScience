- Support vector machines (máquinas de vetores de suporte)
- Em geral é o melhor método
- Reconhecimento facial e de voz (problemas complexos)
- Parecido com Logistic Regression
  . Encontra retas de separação de classes com margem máxima (maior separação entre classes)
- Em uma análise Multivariada essas retas são hiperplanos
- Support vectors são as distâncias dos pontos mais próximos ao hiperplano
- Métodos:  
  . Convex hulls: cascas ou envoltórias convexas
  . vec w . vec x + b = 0 (abordagem matemática)
- Erros e custo (marcadores)
- Problemas tipo AND: linearmente separáveis
- Problemas tipo XOR: não linearmente separáveis
- Não-linear: kernel trick (interpolação? RooKeysPdf?)
- Funções de Kernel: x*y, gaussiana de x, poly (x*y)^a, tanh(x*y-theta)
- Slack variable: novos conceitos a partir dos dados originais
- Mais lento
- Black box
- Influenciável por ruídos
- Sem escalonamento leva muito tempo de processamento pois são muitos cálculos
  a serem realizados
