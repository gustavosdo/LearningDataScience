# lendo os dados
import pandas as pd
dataset = pd.read_csv('risco-credito.csv')

# definindo variaveis para fazer previsao
previsores = dataset.iloc[:,0:4].values
classe = dataset.iloc[:,4].values

# alterando parametros texto para inteiros
from sklearn.preprocessing import LabelEncoder
labelencoder = LabelEncoder()
previsores[:,0] = labelencoder.fit_transform(previsores[:,0])
previsores[:,1] = labelencoder.fit_transform(previsores[:,1])
previsores[:,2] = labelencoder.fit_transform(previsores[:,2])
previsores[:,3] = labelencoder.fit_transform(previsores[:,3])
#print(previsores)

# Algoritmo naive-bayes do scikit-learn
from sklearn.naive_bayes import GaussianNB
classificador = GaussianNB() # pode receber as probabilidades a priori
classificador.fit(previsores,classe) # geracao de tabelas de probabilidades (a priori)

# aplicando tabela de probabilidades ao caso: historia boa, divida alta, garantias nenhuma, renda > 35
# 0012
resultado = classificador.predict([[0,0,1,2]])
print(resultado)

# caso: historia ruim, divida alta, garantias adequada, renda < 15
# 3000
resultado = classificador.predict([[3,0,0,0]])
print(resultado)

# algumas caracteristicas do GaussianNB
print(classificador.classes_) # classes
print(classificador.class_count_) # classes em inteiros
print(classificador.class_prior_) # probs a priori
