# -*- coding: utf-8 -*-

'''
@author: gustavo.if.ufrj@gmail.com
'''

# reading data
import pandas as pd
dataset = pd.read_csv('credit-data.csv')

# non-negative ages mean -> change the negative ones by this mean
age_mean = dataset['age'][dataset.age > 0].mean()
dataset.loc[dataset.age < 0, 'age'] = age_mean

# finding null age cases
#print(dataset.loc[pd.isnull(dataset['age'])])

# splitting farsighted and class variables
farsighted = dataset.iloc[:, 1:4].values # loc with index
class_ = dataset.iloc[:, 4].values

# scikit-learn magic
import numpy as np
from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values=np.nan, strategy='mean') # inputing the mean age in the NaN placeholders
imputer = imputer.fit(farsighted[:,0:3]) # define the place to input
farsighted[:,0:3] = imputer.transform(farsighted[:,0:3]) # performing the subtitution

# attributes scaling
# 1. x = (x - mean_x)/standard_deviation_x (standardisation)
# 2. x = (x - minimum_x)/(maximum_x - minimum_x) (Normalization)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
farsighted = scaler.fit_transform(farsighted)

# split train and test samples
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
farsighted_train, farsighted_test, class_train, class_test = train_test_split(farsighted, class_, test_size=0.25, random_state=0)

# Naive-bayes
from sklearn.naive_bayes import GaussianNB
classificator = GaussianNB()
classificator.fit(farsighted_train, class_train)
previsoes = classificator.predict(farsighted_test)

comparison = list(zip(previsoes,class_test))

fail = 0
success = 0

for item in comparison:
	if (item[0] == item[1]): success += 1
	if (item[0] != item[1]): fail += 1

efficiency = 1.*success/(success+fail)

print(success)
print(fail)
print(efficiency)

# usando scikit-learn
from sklearn.metrics import confusion_matrix, accuracy_score
precisao = accuracy_score(class_test,previsoes)
matriz = confusion_matrix(class_test,previsoes)
print(precisao)
print(matriz)
