# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.DESCRICAO DO CODIGO # # # # # # # # # #
# Autor: gustavo.if.ufrj at gmail . com
# Amostra: gerada pelo codigo
# Objetivo: solucionar os problemas do capitulo 4 do curso
# de fundamentos de Python para analise de dados

# # # # # # # # # # 2.BIBLIOTECAS # # # # # # # # # #
import pandas as pd
from random import randint
from random_word import RandomWords

# # # # # # # # # # 3.FUNCOES GLOBAIS # # # # # # # # # #
questoes = [\
"Exercício 1 - Crie um objeto a partir da classe abaixo, chamado roc1, passando 2 parâmetros e depois faça uma chamada aos atributos e métodos\nfrom math import sqrt\nclass Rocket():\n    def __init__(self, x=0, y=0):\n        self.x = x\n        self.y = y\n    def move_rocket(self, x_increment=0, y_increment=1):\n        self.x += x_increment\n        self.y += y_increment\n    def print_rocket(self):\n        print(self.x, self.y)",\
"Exercício 2 - Crie uma classe chamada Pessoa() com os atributos: nome, cidade, telefone e e-mail. Use pelo menos 2 métodos especiais na sua classe. Crie um objeto da sua classe e faça uma chamada a pelo menos um dos seus métodos especiais.",\
"Exercício 3 - Crie a classe Smartphone com 2 atributos, tamanho e interface e crie a classe MP3Player com os atributos capacidade. A classe MP3player deve herdar os atributos da classe Smartphone.",\
]

def imprimeEnunciado(num_questao):
	print("\n - - - - - - - - - - - - - - - - - - - ")
	print(questoes[num_questao-1])

# # # # # # # # # # 4.CODIGO DA SOLUCAO # # # # # # # # # #
class ExerciciosCap4():
	def __init__(self):
		pass

	def questao1(self):
		imprimeEnunciado(1)
		class Rocket():
			def __init__(self, x=0, y=0):
				self.x = x
				self.y = y
			def move_rocket(self, x_increment=0, y_increment=1):
				self.x += x_increment
				self.y += y_increment
			def print_rocket(self):
				print(self.x, self.y)
		#
		roc1 = Rocket(randint(0,5),randint(0,5))
		print("roc1.x = %d" %roc1.x)
		print("roc1.y = %d" %roc1.y)
		roc1.move_rocket()
		roc1.print_rocket()

	def questao2(self):
		imprimeEnunciado(2)
		class Pessoa():
			def __init__(self, nome = '', cidade = '', telefone = '', email = ''):
				self.name = nome
				self.city = cidade
				self.phone = telefone
				self.mail = email
			def __str__(self):
				return("Classe que define uma pessoa a partir das seguintes informações: nome, cidade, telefone e email")
		#
		Adao = Pessoa('Adão', 'Éden', '00000001', 'adao@eden.com')
		print(Adao)

	def questao3(self):
		imprimeEnunciado(3)
		class Smartphone():
			def __init__(self, tamanho='0x0', interface = 'Android'):
				self.size = tamanho
				self.ui = interface
		class MP3player(Smartphone):
			def __init__(self, tamanho = '', interface = '', capacidade = '2GB'):
				Smartphone.__init__(self,tamanho,interface)
				self.storage = capacidade
			def __str__(self):
				return("MP3player de tamanho %s, interface %s e capacidade %s" %(self.size,self.ui,self.storage))
		iPod = MP3player('5x5', 'iOS', '70GB')
		print(iPod)

# # # # # # # # # # 5.SOLUCAO # # # # # # # # # #
Solucao = ExerciciosCap4()
Solucao.questao1()
Solucao.questao2()
Solucao.questao3()
