# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.DESCRICAO DO CODIGO # # # # # # # # # #
# Autor: gustavo.if.ufrj at gmail . com
# Amostra: gerada pelo codigo
# Objetivo: solucionar os problemas do capitulo 4 do curso
# de fundamentos de Python para analise de dados

# # # # # # # # # # 2.BIBLIOTECAS # # # # # # # # # #
import pandas as pd
from random import randint
from random_word import RandomWords

# # # # # # # # # # 3.FUNCOES GLOBAIS # # # # # # # # # #
questoes = [\
"Exercício 1 - Crie uma lista de 3 elementos e calcule a terceira potência de cada elemento.",\
"Exercício 2 - Reescreva o código abaixo, usando a função map(). O resultado final deve ser o mesmo!\npalavras = 'A Data Science Academy oferce os melhores cursos de análise de dados do Brasil'.split()\nresultado = [[w.upper(), w.lower(), len(w)] for w in palavras]\nfor i in resultado:\n    print (i)\n",\
"Exercício 3 - Calcule a matriz transposta da matriz abaixo.\nCaso não saiba o que é matriz transposta, visite este link: https://pt.wikipedia.org/wiki/Matriz_transposta\nMatriz transposta é um conceito fundamental na construção de redes neurais artificiais, base de sistemas de IA.\nmatrix = [[1, 2],[3,4],[5,6],[7,8]]",\
"Exercício 4 - Crie duas funções, uma para elevar um número ao quadrado e outra para elevar ao cubo.\nAplique as duas funções aos elementos da lista abaixo.\nObs: as duas funções devem ser aplicadas simultaneamente.\nlista = [0, 1, 2, 3, 4]\n",\
"Exercício 5 - Abaixo você encontra duas listas. Faça com que cada elemento da listaA seja elevado\nao elemento correspondente na listaB.\nlistaA = [2, 3, 4]\nlistaB = [10, 11, 12]",\
"Exercício 6 - Considerando o range de valores abaixo, use a função filter() para retornar apenas os valores negativos.\nrange(-5, 5)",\
"Exercício 7 - Usando a função filter(), encontre os valores que são comuns às duas listas abaixo.\na = [1,2,3,5,7,9]\nb = [2,3,5,6,7,8]",\
"Exercício 8 - Considere o código abaixo. Obtenha o mesmo resultado usando o pacote time.\nNão conhece o pacote time? Pesquise!\nimport datetime\nprint (datetime.datetime.now().strftime(\"%d/%m/%Y %H:%M\"))",\
"Exercício 9 - Considere os dois dicionários abaixo.\nCrie um terceiro dicionário com as chaves do dicionário 1 e os valores do dicionário 2.\ndict1 = {'a':1,'b':2}\ndict2 = {'c':4,'d':5}",\
"Exercício 10 - Considere a lista abaixo e retorne apenas os elementos cujo índice for maior que 5.\nlista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']",\
]

def imprimeEnunciado(num_questao):
	print("\n - - - - - - - - - - - - - - - - - - - ")
	print(questoes[num_questao-1])

# # # # # # # # # # 4.CODIGO DA SOLUCAO # # # # # # # # # #
class ExerciciosCap4():
	def __init__(self):
		pass

	def questao1(self):
		imprimeEnunciado(1)
		lista = [randint(-5,5) for i in range(0,3)]
		print(lista)
		for item in lista: print(item**3)

	def questao2(self):
		imprimeEnunciado(2)
		palavras = 'A Data Science Academy oferce os melhores cursos de análise de dados do Brasil'.split()
		resultado = [[w.upper(), w.lower(), len(w)] for w in palavras]
		for i in resultado:
			print (i)
		resultado_map = list(map(lambda i: [i.upper(),i.lower(),len(i)],palavras))
		for i in resultado_map:
			print (i)

	def questao3(self):
		imprimeEnunciado(3)
		# Função que transpõe matrizes de qualquer ordem
		def transporMatriz(A):
			m = len(A)
			n = len(A[0])
			At = [ [0 for j in range(0,m)] for i in range(0,n)]
			for i in range(0,n):
				for j in range(0,m):
					At[i][j] = A[j][i]
			return(At)
		A = [[1, 2],[3,4],[5,6],[7,8]]
		print(transporMatriz(A))

	def questao4(self):
		imprimeEnunciado(4)
		lista = [0, 1, 2, 3, 4]
		resultado = list(map(lambda x: [x*x, x*x*x], lista))
		print(resultado)

	def questao5(self):
		imprimeEnunciado(5)
		listaA = [2, 3, 4]
		listaB = [10, 11, 12]
		resultado = list(map(lambda x,a:x**a, listaA, listaB))
		print(resultado)

	def questao6(self):
		imprimeEnunciado(6)
		print( list( filter( lambda x: x>0, [y for y in range(-5,5)] ) ) )

	def questao7(self):
		imprimeEnunciado(7)
		a = [1,2,3,5,7,9]
		b = [2,3,5,6,7,8]
		resultado = []
		for x in a:
			if list(filter( lambda y: y == x, b )) != []:
				resultado.append(x)

	def questao8(self):
		imprimeEnunciado(8)
		import datetime
		print (datetime.datetime.now().strftime("%d/%m/%Y %H:%M"))
		import time
		print (time.strftime("%d/%m/%Y %H:%M"))

	def questao9(self):
		imprimeEnunciado(9)
		dict1 = {'a':1,'b':2}
		dict2 = {'c':4,'d':5}
		#print(dict1.keys())
		#print(dict2.values())
		resultado = dict(zip(dict1.keys(),dict2.values()))
		print(resultado)

	def questao10(self):
		imprimeEnunciado(10)
		lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
		resultado = list ( filter ( lambda c: lista.index(c) > 5, lista ) )
		print(resultado)

# # # # # # # # # # 5.SOLUCAO # # # # # # # # # #
Solucao = ExerciciosCap4()
Solucao.questao1()
Solucao.questao2()
Solucao.questao3()
Solucao.questao4()
Solucao.questao5()
Solucao.questao6()
Solucao.questao7()
Solucao.questao8()
Solucao.questao9()
Solucao.questao10()
