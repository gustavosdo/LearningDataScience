# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.DESCRICAO DO CODIGO # # # # # # # # # #
# Autor: gustavo.if.ufrj at gmail . com
# Amostra: gerada pelo codigo
# Objetivo: solucionar os problemas do capitulo 3 do curso
# de fundamentos de Python para analise de dados

# # # # # # # # # # 2.BIBLIOTECAS # # # # # # # # # #
import pandas as pd
from random import randint
from random_word import RandomWords

# # # # # # # # # # 3.FUNCOES GLOBAIS # # # # # # # # # #
questoes = ["Exercício 1 - Crie uma função que imprima a sequência de números pares entre 1 e 20 (a função não recebe parâmetro) e depois faça uma chamada à função para listar os números",\
"Exercício 2 - Crie uma função que receba uma string como argumento e retorne a mesma string em letras maiúsculas. Faça uma chamada à função, passando como parâmetro uma string",\
"Exercício 3 - Crie uma função que receba como parâmetro uma lista de 4 elementos, adicione 2 elementos a lista e imprima a lista",\
"Exercício 4 - Crie uma função que receba um argumento formal e uma possível lista de elementos. Faça duas chamadas à função, com apenas 1 elemento e na segunda chamada com 4 elementos",\
"Exercício 5 - Crie uma função anônima e atribua seu retorno a uma variável chamada soma. A expressão vai receber 2 números como parâmetro e retornar a soma deles",\
"Exercício 6 - Execute o código abaixo e certifique-se que compreende a diferença entre variável global e local:\ntotal = 0\ndef soma(arg1, arg2):\ntotal = arg1 + arg2\nprint (\"Dentro da função o total é: \", total)\nreturn total\n\nsoma( 10, 20 )\nprint (\"Fora da função o total é: \", total)",\
"Exercício 7 - Abaixo você encontra uma lista com temperaturas em graus Celsius\nCrie uma função anônima que converta cada temperatura para Fahrenheit\nDica: para conseguir realizar este exercício, você deve criar sua função lambda, dentro de uma função\n(que será estudada no próximo capítulo). Isso permite aplicar sua função a cada elemento da lista\nComo descobrir a fórmula matemática que converte de Celsius para Fahrenheit? Pesquise!!!\nCelsius = [39.2, 36.5, 37.3, 37.8]\nFahrenheit = map(coloque_aqui_sua_função_lambda)\nprint (list(Fahrenheit))",\
"Exercício 8 - Crie um dicionário e liste todos os métodos e atributos do dicionário",\
"Exercício 9 - Abaixo você encontra a importação do Pandas, um dos principais pacotes Python para análise de dados. Analise atentamente todos os métodos disponíveis. Um deles você vai usar no próximo exercício.\nimport pandas as pd\ndir(pd)",\
"Exercício 10 - Crie uma função que receba o arquivo abaixo como argumento e retorne um resumo estatístico descritivo do arquivo. Dica, use Pandas e um de seus métodos, describe()\nArquivo: \"binary.csv\"\nimport pandas as pd\nfile_name = \"binary.csv\"",\
"Exercício 11 - Crie uma estrutura que pergunte ao usuário qual o dia da semana. Se o dia for igual a Domingo ou igual a sábado, imprima na tela \"Hoje é dia de descanso\", caso contrário imprima na tela \"Você precisa trabalhar!\"",\
"Exercício 12 - Crie uma lista de 5 frutas e verifique se a fruta 'Morango' faz parte da lista",\
"Exercício 13 - Crie uma tupla de 4 elementos, multiplique cada elemento da tupla por 2 e guarde os resultados em uma lista",\
"Exercício 14 - Crie uma sequência de números pares entre 100 e 150 e imprima na tela",\
"Exercício 15 - Crie uma variável chamada temperatura e atribua o valor 40. Enquanto temperatura for maior que 35, imprima as temperaturas na tela",\
"Exercício 16 - Crie uma variável chamada contador = 0. Enquanto counter for menor que 100, imprima os valores na tela, mas quando for encontrado o valor 23, interrompa a execução do programa",\
"Exercício 17 - Crie uma lista vazia e uma variável com valor 4. Enquanto o valor da variável for menor ou igual a 20, adicione à lista, apenas os valores pares e imprima a lista",\
"Exercício 18 - Transforme o resultado desta função range em uma lista: range(5, 45, 2)",\
"Exercício 19 - Faça a correção dos erros no código abaixo e execute o programa. Dica: são 3 erros.\ntemperatura = float(input(\'Qual a temperatura? \'))\nif temperatura > 30\nprint('Vista roupas leves.')\nelse\n    print('Busque seus casacos.')",\
"Exercício 20 - Faça um programa que conte quantas vezes a letra \"r\" aparece na frase abaixo. Use um placeholder na sua instrução de impressão\n\"É melhor, muito melhor, contentar-se com a realidade; se ela não é tão brilhante como os sonhos, tem pelo menos a vantagem de existir.\" (Machado de Assis)"]

def imprimeEnunciado(num_questao):
	print("\n - - - - - - - - - - - - - - - - - - - ")
	print(questoes[num_questao-1])

def isNumber_lista(lista):
	return(filter(lambda a: True if (type(a) == int or type(a) == float) else False, lista))
	
isNumber = lambda a: True if (type(a) == int or type(a) == float) else False

soma = lambda a,b: a+b

# # # # # # # # # # 4.CODIGO DA SOLUCAO # # # # # # # # # #
class ExerciciosCap3():
	def __init__(self):
		pass

	def questao1(self):
		imprimeEnunciado(1)
		pares = []
		for i in range(1,11):
			pares.append(2*i)
		print(pares)

	def questao2(self, str_q2):
		imprimeEnunciado(2)
		print(str_q2.upper())

	def questao3(self, lista):
		imprimeEnunciado(3)
		lista.append(randint(-5, 5))
		lista.append(randint(-6, 6))
		print(lista)
		
	def questao4(self, arg1, *lista):
		imprimeEnunciado(4)
		n = 0
		if isNumber(arg1) == True: n = n + 1
		for i in lista:
			if isNumber(i) == True: n = n + 1
		print('Quantidade de números (float ou int): ', n)

	def questao5(self):
		imprimeEnunciado(5)
		print("soma(2,3) = ", soma(2,3))

	def questao6(self):
		imprimeEnunciado(6)
		total = 0
		def soma(arg1, arg2):
			total = arg1 + arg2
			print("Dentro da função o total é: ", total)
			return total
		soma( 10, 20 )
		print("Fora da função o total é: ", total)
		print("Resposta: \"total = 0\" antes da definição de soma(arg1,arg2) é uma variável local; \"total = arg1 + arg2\", apesar do mesmo nome, é outra variável definida apenas dentro da função e alocada em outro espaço de memória")

	def questao7(self):
		imprimeEnunciado(7)
		Celsius = [39.2, 36.5, 37.3, 37.8]
		Fahrenheit = map(lambda Tc: 1.8*Tc + 32, Celsius)
		print(list(Fahrenheit))
		
	def questao8(self):
		imprimeEnunciado(8)
		dicionario = {1:'a', 2:'b'}
		print(dir(dicionario))

	def questao9(self):
		imprimeEnunciado(9)
		print(dir(pd))

	def questao10(self):
		imprimeEnunciado(10)
		file_name = "binary.csv"
		data = pd.read_csv(file_name)
		print(data.describe())
		print(type(data.describe()))

	def questao11(self):
		imprimeEnunciado(11)
		dias = ['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado', 'domingo']
		dia = ''
		while dia not in dias:
			dia = str(input('Hoje é que dia da semana? '))
			dia = dia.lower()
		if dia in ['sábado', 'domingo']:
			print('Hoje é dia de descanso')
		else: print('Você precisa trabalhar!')

	def questao12(self):
		imprimeEnunciado(12)
		frutas = [open('frutas.txt', 'r').readlines()[randint(0,207)].rstrip("\n") for i in range(0,5)]
		print(frutas)
		if 'Morango' in frutas:
			print('Morango está na posição ', frutas.index('Morango'))
		else:
			print('Morango não está na lista')

	def questao13(self):
		imprimeEnunciado(13)
		lista = [randint(-9,9) for i in range(0,4)]
		print(lista)
		tupla = tuple(lista)
		print(tupla)
		lista = [4*x for x in tupla]
		print(lista)

	def questao14(self):
		imprimeEnunciado(14)
		pares = []
		for i in range(50,76):
			pares.append(2*i)
		print(pares)

	def questao15(self):
		imprimeEnunciado(15)
		temperatura = 40
		while temperatura > 35:
			print("Temperatura = "+str(temperatura))
			temperatura -= 1

	def questao16(self):
		imprimeEnunciado(16)
		counter = 0
		while counter < 100:
			print("Counter = "+str(counter))
			if counter == 23:
				break
			counter += 1

	def questao17(self):
		imprimeEnunciado(17)
		lista = []
		num = 4
		while num <= 20:
			if num%2 == 0:
				lista.append(num)
			num += 1
		print(lista)

	def questao18(self):
		imprimeEnunciado(18)
		lista = [x for x in range(5, 45, 2)]
		print(lista)

	def questao19(self):
		imprimeEnunciado(19)
		print('Faltam dois pontos no final da segunda linha')
		print('Primeiro print estava sem identação')
		print('Faltam dois pontos depois do else')
		temperatura = float(input('Qual a temperatura? '))
		if temperatura > 30:
			print('Vista roupas leves.')
		else:
			print('Busque seus casacos.')

	def questao20(self):
		imprimeEnunciado(20)
		frase = 'É melhor, muito melhor, contentar-se com a realidade; se ela não é tão brilhante como os sonhos, tem pelo menos a vantagem de existir.'
		n_r = 0
		for letra in frase:
			if letra == 'r': n_r += 1
		print('Há %d r\'s na frase' %(n_r))

# # # # # # # # # # 5.SOLUCAO # # # # # # # # # #
Solucao = ExerciciosCap3()
Solucao.questao1()
Solucao.questao2(RandomWords().get_random_word()+' '+RandomWords().get_random_word())
Solucao.questao3([randint(-i, i) for i in range(1,5)])
Solucao.questao4(1)
Solucao.questao4(2, 'z', [-10, 10], 3)
Solucao.questao5()
Solucao.questao6()
Solucao.questao7()
Solucao.questao8()
Solucao.questao9()
Solucao.questao10()
Solucao.questao11()
Solucao.questao12()
Solucao.questao13()
Solucao.questao14()
Solucao.questao15()
Solucao.questao16()
Solucao.questao17()
Solucao.questao18()
Solucao.questao19()
Solucao.questao20()
