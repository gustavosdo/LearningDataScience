# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.DESCRICAO DO CODIGO # # # # # # # # # #
# Autor: gustavo.if.ufrj at gmail . com
# Amostra: gerada pelo codigo
# Objetivo: solucionar os problemas do capitulo 4 do curso
# de fundamentos de Python para analise de dados

# # # # # # # # # # 2.BIBLIOTECAS # # # # # # # # # #
import pandas as pd
import numpy as np
import time
from random import randint
from random_word import RandomWords

# # # # # # # # # # 3.FUNCOES GLOBAIS # # # # # # # # # #
questoes = [\
'Crie um array NumPy com 1000000 e uma lista com 1000000. Multiplique cada elemento do array e da lista por 2 e calcule o tempo de execução com cada um dos objetos (use %time). Qual objeto oferece melhor performance, array NumPy ou lista?',\
'Crie um array de 10 elementos. Altere o valores de todos os elementos dos índices 5 a 8 para 0',\
'Crie um array de 3 dimensões e imprima a dimensão 1',\
'Crie um array de duas dimensões (matriz). Imprima os elementos da terceira linha da matriz. Imprima todos os elementos da primeira e segunda linhas e segunda e terceira colunas',\
'Calcule a transposta da matriz abaixo\narr = np.arange(15).reshape((3, 5))',\
'Considere os 3 arrays abaixo. Retorne o valor do array xarr se o valor for True no array cond. Caso contrário, retorne o valor do array yarr.\nxarr = np.array([1.1, 1.2, 1.3, 1.4, 1.5])\nyarr = np.array([2.1, 2.2, 2.3, 2.4, 2.5])\ncond = np.array([True, False, True, True, False])',\
'Crie um array A com 10 elementos e salve o array em disco com a extensão npy. Depois carregue o array do disco no array B',\
'Considerando a série abaixo, imprima os valores únicos na série\nobj = pd.Series([\'c\', \'a\', \'d\', \'a\', \'a\', \'b\', \'b\', \'c\', \'c\', \'a\', \'b\'])',\
'Considerando o trecho de código que conecta em uma url na internet, imprima o dataframe conforme abaixo.\nimport requests\nurl = \'https://api.github.com/repos/pandas-dev/pandas/issues\'\nresp = requests.get(url)',\
'Crie um banco de dados no SQLite, crie uma tabela, insira registros, consulte a tabela e retorne os dados em dataframe do Pandas',\
]

def imprimeEnunciado(num_questao):
    print("\n - - - - - - - - - - - - - - - - - - - ")
    print(questoes[num_questao-1])

# # # # # # # # # # 4.CODIGO DA SOLUCAO # # # # # # # # # #
class ExerciciosCap8():
    def __init__(self):
        pass

    def questao1(self):
        imprimeEnunciado(1)
        start = time.time()
        npArray = np.zeros(1000000)
        end = time.time()
        print(end-start)
        start = time.time()
        pyList = [0 for i in range(1000000)]
        end = time.time()
        print(end-start)

    def questao2(self):
        imprimeEnunciado(2)
        npArray = np.array([randint(1, 100) for i in range(10)])
        print(npArray)
        npArray[5:8] = 0
        print(npArray)

    def questao3(self):
        imprimeEnunciado(3)
        npArray = np.array([[randint(1, 100) for i in range(3)], [randint(-100, 0) for i in range(3)], [randint(-50, 50) for i in range(3)]])
        print(npArray)
        print(npArray[0])

    def questao4(self):
        imprimeEnunciado(4)
        npMatrix = np.matrix([[randint(1, 100) for i in range(5)], [randint(-100, 0) for i in range(5)], [randint(-1, 1) for i in range(5)]])
        print(npMatrix)

    def questao5(self):
        imprimeEnunciado(5)
        arr = np.arange(15).reshape((3, 5))
        print(arr)
        tra = arr.transpose()
        print(tra)

    def questao6(self):
        imprimeEnunciado(6)
        xarr = np.array([1.1, 1.2, 1.3, 1.4, 1.5])
        yarr = np.array([2.1, 2.2, 2.3, 2.4, 2.5])
        cond = np.array([True, False, True, True, False])
        for i in range(cond.size):
            boolean = cond[i]
            if (boolean):
                print(xarr[i])
            else:
                print(yarr[i])

    def questao7(self):
        imprimeEnunciado(7)
        A = np.arange(10)
        np.save(file = 'npArray_q7.npy', arr = A)
        B = np.load(file = 'npArray_q7.npy')
        print(B)
        

    def questao8(self):
        imprimeEnunciado(8)
        obj = pd.Series(['c', 'a', 'd', 'a', 'a', 'b', 'b', 'c', 'c', 'a', 'b'])
        obj_unique = pd.unique(obj)
        print(obj_unique)

    def questao9(self):
        imprimeEnunciado(9)
        import requests
        url = 'https://api.github.com/repos/pandas-dev/pandas/issues'
        resp = requests.get(url)
        df = pd.DataFrame(resp)
        #print(df.iloc[0,0])
        print(df)

    def questao10(self):
        imprimeEnunciado(10)
        

# # # # # # # # # # 5.SOLUCAO # # # # # # # # # #
Solucao = ExerciciosCap8()
Solucao.questao1()
Solucao.questao2()
Solucao.questao3()
Solucao.questao4()
Solucao.questao5()
Solucao.questao6()
Solucao.questao7()
Solucao.questao8()
Solucao.questao9()
Solucao.questao10()
