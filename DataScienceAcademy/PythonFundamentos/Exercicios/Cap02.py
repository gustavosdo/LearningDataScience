# - *- coding: utf- 8 - *-

# # # # # # # # # # 1.DESCRICAO DO CODIGO # # # # # # # # # #
# Autor: gustavo.if.ufrj at gmail . com
# Amostra: gerada pelo codigo
# Objetivo: solucionar os problemas do cap2 do curso
# de fundamentos de Python para analise de dados

# # # # # # # # # # 2.BIBLIOTECAS # # # # # # # # # #
from random import randint
from random_word import RandomWords

# # # # # # # # # # 3.FUNCOES GLOBAIS # # # # # # # # # #
questoes = [\
"Exercício 1 - Imprima na tela os números de 1 a 10. Use uma lista para armazenar os números.",\
"Exercício 2 - Crie uma lista de 5 objetos e imprima na tela",\
"Exercício 3 - Crie duas strings e concatene as duas em uma terceira string",\
"Exercício 4 - Crie uma tupla com os seguintes elementos: 1, 2, 2, 3, 4, 4, 4, 5 e depois utilize a função count do  objeto tupla para verificar quantas vezes o número 4 aparece na tupla",\
"Exercício 5 - Crie um dicionário vazio e imprima na tela",\
"Exercício 6 - Crie um dicionário com 3 chaves e 3 valores e imprima na tela",\
"Exercício 7 - Adicione mais um elemento ao dicionário criado no exercício anterior e imprima na tela",\
"Exercício 8 - Crie um dicionário com 3 chaves e 3 valores. Um dos valores deve ser uma lista de 2 elementos numéricos. Imprima o dicionário na tela.",\
"Exercício 9 - Crie uma lista de 4 elementos. O primeiro elemento deve ser uma string, o segundo uma tupla de 2 elementos, o terceiro um dcionário com 2 chaves e 2 valores e o quarto elemento um valor do tipo float. Imprima a lista na tela.",\
"Exercício 10 - Considere a string abaixo. Imprima na tela apenas os caracteres da posição 1 a 18. frase = 'Cientista de Dados é o profissional mais sexy do século XXI'"]

def imprimeEnunciado(num_questao):
	print("\n - - - - - - - - - - - - - - - - - - - ")
	print(questoes[num_questao-1])

# # # # # # # # # # 4.CODIGO DA SOLUCAO # # # # # # # # # #
class ExerciciosCap2():
	def __init__(self, dicionario = {}):
		self.dicionario = dicionario

	def questao1(self):
		imprimeEnunciado(1)
		lista = []
		for i in range(1,11):
			print(i)
			lista.append(i)
		print(lista) # descomente para testar

	def questao2(self):
		imprimeEnunciado(2)
		lista = []
		for i in range(0,5):
			lista.append(randint(-100,100))
		print(lista)

	def questao3(self):
		imprimeEnunciado(3)
		str1 = " Essa frase"
		str2 = " está concatenada"
		str3 = str1+str2
		print(str3)

	def questao4(self):
		imprimeEnunciado(4)
		tupla = (1, 2, 2, 3, 4, 4, 4, 5)
		n4 = tupla.count(4)
		print("Número de vezes que aparece o quatro na tupla é: ", n4)

	def questao5(self):
		imprimeEnunciado(5)
		print(self.dicionario)
		
	def questao6(self):
		imprimeEnunciado(6)
		for i in range(0,3):
			word = RandomWords().get_random_word()
			num = randint(-100,100)
			self.dicionario[num] = word
		print(self.dicionario)

	def questao7(self):
		imprimeEnunciado(7)
		self.dicionario[-999] = 'new word'
		print(self.dicionario)

	def questao8(self):
		imprimeEnunciado(8)
		self.dicionario = {}
		for i in range(0,3):
			#word = RandomWords().get_random_word()
			num = randint(-100,100)
			if (i==1):
				self.dicionario[num] = [randint(-100,-1), randint(1,100)]
			else:
				self.dicionario[num] = RandomWords().get_random_word()
		print(self.dicionario)

	def questao9(self):
		imprimeEnunciado(9)
		lista = ['string rá', ('1', 2), {1:'um', '2':'dois'}, 3.14]
		print(lista)

	def questao10(self):
		imprimeEnunciado(10)
		string = 'Cientista de Dados é o profissional mais sexy do século XXI'
		print(string[0:18])

# # # # # # # # # # 5.SOLUCAO # # # # # # # # # #
Solucao = ExerciciosCap2()
Solucao.questao1()
Solucao.questao2()
Solucao.questao3()
Solucao.questao4()
Solucao.questao5()
Solucao.questao6()
Solucao.questao7()
Solucao.questao8()
Solucao.questao9()
Solucao.questao10()
