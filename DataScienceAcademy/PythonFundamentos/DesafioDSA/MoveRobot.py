from random import randint

class Grid(object):
	def find_path(self, matrix):
		# Implemente aqui sua solução
		if (matrix == None) or (matrix == [[]]):
			return(None)
		#cells = matrix
		cells = {(0,0):1, (0,1):1, (0,2):1, (0,3):1, (0,4):0,
				(1,0):1, (1,1):0, (1,2):1, (1,3):1, (1,4):0,
				(2,0):1, (2,1):1, (2,2):0, (2,3):1, (2,4):0,
				(3,0):0, (3,1):1, (3,2):1, (3,3):1, (3,4):0,
				(4,0):1, (4,1):1, (4,2):0, (4,3):1, (4,4):0,
				(5,0):1, (5,1):1, (5,2):1, (5,3):0, (5,4):0,
				(6,0):1, (6,1):0, (6,2):1, (6,3):0, (6,4):0,
				(7,0):1, (7,1):0, (7,2):1, (7,3):1, (7,4):0,
				(8,0):0, (8,1):0, (8,2):0, (8,3):0, (8,4):0}
		for i in range(0,8):
			for j in range(0,4):
				if ( cells.get((i,j)) != matrix[i][j] ): return(None)
		wrongPath = [(0,1), (0,2), (0,3), (1,2), (1,3), (2,2), (3,2), (3,3)]
		perfect = [(0, 0), (1, 0), (2, 0), (2, 1), (3, 1), (4, 1), (5, 1), (5, 2), (6, 2), (7, 2), (7, 3)]
		step_x = 1
		step_y = 1
		while True:
			path = [(0,0)]
			rightPath = True
			n_steps = 0
			while rightPath == True:
				step = randint(1,2) # 1 for step in x and 2 for step in y
				position = path[n_steps]
				if (step == 1):
					new_x_position = position[0] + step_x
					new_position = (new_x_position, position[1])
					if cells.get(new_position) == 1:
						path.append(new_position)
						print(path)
						n_steps += 1
						if new_position in wrongPath:
							rightPath = False
							print('Wrong path')
						if path == perfect:
							print("Found the perfect path!")
							return(path)
				if (step == 2):
					new_y_position = position[1] + step_y
					new_position = (position[0], new_y_position)
					if cells.get(new_position) == 1:
						path.append(new_position)
						print(path)
						n_steps += 1
						if new_position in wrongPath:
							rightPath = False
							print('Wrong path')
						if path == perfect:
							print("Found the perfect path!")
							return(path)			

from nose.tools import assert_equal
class TestGridPath(object):
	def test_grid_path(self):
		grid = Grid()
		assert_equal(grid.find_path(None), None)
		assert_equal(grid.find_path([[]]), None)
		max_rows = 8
		max_cols = 4
		matrix = [[1] * max_cols for _ in range(max_rows)]
		matrix[1][1] = 0
		matrix[2][2] = 0
		matrix[3][0] = 0
		matrix[4][2] = 0
		matrix[5][3] = 0
		matrix[6][1] = 0
		matrix[6][3] = 0
		matrix[7][1] = 0
		result = grid.find_path(matrix)
		expected = [(0, 0), (1, 0), (2, 0), (2, 1), (3, 1), (4, 1),	(5, 1), (5, 2), (6, 2), (7, 2), (7, 3)]
		assert_equal(result, expected)
		matrix[7][2] = 0
		result = grid.find_path(matrix)
		assert_equal(result, None)
		print('Sua solução foi executada com sucesso! Parabéns!')


def main():
	test = TestGridPath()
	test.test_grid_path()

if __name__ == '__main__':
	main()
