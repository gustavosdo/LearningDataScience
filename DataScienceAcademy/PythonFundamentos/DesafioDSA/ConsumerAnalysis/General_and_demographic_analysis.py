# Importing required libraries
import pandas as pd
import numpy as np
from scipy.stats import mode
import warnings
warnings.filterwarnings("ignore") # ignoring all warnings

# Loading original dataset
FileName = "data.json"
dataset = pd.read_json(FileName, orient = "records")

# # General analysis
totalConsumers = len(dataset.Login.unique())
#
print('Total number of clients is: ', totalConsumers)
print('Number of exclusive products is: ', len(dataset.loc[:,'Nome do Item'].unique()))
print('Mean value of purchase: %.2f' %np.mean(dataset.Valor))
print('Number of purchases: ', len(dataset.iloc[:,0]))
print('Total income: ', np.sum(dataset.Valor))

# # Gender demographics
males = dataset[dataset.Sexo == 'Masculino']
females = dataset[dataset.Sexo == 'Feminino']
others = dataset[(dataset.Sexo != 'Masculino') & (dataset.Sexo != 'Feminino')]
#
sumMales = len(males.Login.unique()) ; malesPercent = 100.*sumMales/totalConsumers
sumFemales = len(females.Login.unique()) ; femalesPercent = 100.*sumFemales/totalConsumers
sumOthers = len(others.Login.unique()) ; othersPercent = 100.*sumOthers/totalConsumers
print('There are %d male consumers, they correspond approx. for %.4f%% of total consumers' %(sumMales, malesPercent))
print('There are %d female consumers, they correspond approx. for %.4f%% of total consumers' %(sumFemales, femalesPercent))
print('There are %d others consumers, they correspond approx. for %.4f%% of total consumers' %(sumOthers, othersPercent))

# # Purchase analysis by gender
print("There were %d purchases made by males" %(len(males)))
print("There were %d purchases made by females" %(len(females)))
print("There were %d purchases made by others" %(len(others)))
print("The mean transaction value for males is approx. %.2f" %np.mean(males.Valor))
print("The mean transaction value for females is approx. %.2f" %np.mean(females.Valor))
print("The mean transaction value for others is approx. %.2f" %np.mean(others.Valor))
print("The total income from males is approx. %.2f" %np.sum(males.Valor))
print("The total income from females is approx. %.2f" %np.sum(females.Valor))
print("The total income from others is approx. %.2f" %np.sum(others.Valor))
#
ages = dataset.Idade
deltaAge = (max(list(ages)) - min(list(ages)))/3.
#
males1 = males[(dataset.Idade >= min(list(ages))) & (dataset.Idade <= min(list(ages)) + deltaAge)]
males2 = males[(dataset.Idade >= min(list(ages)) + deltaAge) & (dataset.Idade <= min(list(ages)) + 2*deltaAge)]
males3 = males[(dataset.Idade >= min(list(ages)) + 2*deltaAge) & (dataset.Idade <= min(list(ages)) + 3*deltaAge)]
print("#purchases(male, age between 7 and 19): ", len(males1))
print("#purchases(male, age between 20 and 32): ", len(males2))
print("#purchases(male, age between 33 and 45): ", len(males3))
#
females1 = females[(dataset.Idade >= min(list(ages))) & (dataset.Idade <= min(list(ages)) + deltaAge)]
females2 = females[(dataset.Idade >= min(list(ages)) + deltaAge) & (dataset.Idade <= min(list(ages)) + 2*deltaAge)]
females3 = females[(dataset.Idade >= min(list(ages)) + 2*deltaAge) & (dataset.Idade <= min(list(ages)) + 3*deltaAge)]
print("#purchases(female, age between 7 and 19): ", len(females1))
print("#purchases(female, age between 20 and 32): ", len(females2))
print("#purchases(female, age between 33 and 45): ", len(females3))
#
others1 = others[(dataset.Idade >= min(list(ages))) & (dataset.Idade <= min(list(ages)) + deltaAge)]
others2 = others[(dataset.Idade >= min(list(ages)) + deltaAge) & (dataset.Idade <= min(list(ages)) + 2*deltaAge)]
others3 = others[(dataset.Idade >= min(list(ages)) + 2*deltaAge) & (dataset.Idade <= min(list(ages)) + 3*deltaAge)]
print("#purchases(other, age between 7 and 19): ", len(others1))
print("#purchases(other, age between 20 and 32): ", len(others2))
print("#purchases(other, age between 33 and 45): ", len(others3))
