# Importing required libraries
import pandas as pd
import numpy as np

# Loading original dataset
FileName = "data.json"
dataset = pd.read_json(FileName, orient = "records")

print(dataset.head())

print(len(dataset[dataset.Sexo == 'Masculino'].Login.unique()))
print(len(dataset[dataset.Sexo == 'Feminino'].Login.unique()))
print(len(dataset[(dataset.Sexo != 'Masculino') & (dataset.Sexo != 'Feminino')].Login.unique()))

