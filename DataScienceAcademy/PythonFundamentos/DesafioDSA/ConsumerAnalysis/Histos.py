# Importing required libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import warnings
warnings.filterwarnings("ignore") # ignoring all warnings

# Loading original dataset
FileName = "data.json"
dataset = pd.read_json(FileName, orient = "records")

# Purchase quantity by age (histogram)
plt.hist(dataset.Idade, bins = [i for i in range(6,46)], normed=True)
plt.xlabel('Age (years)')
plt.ylabel('Number of purchases (normalized)')
# Fitting gaussian
mean, sigma = norm.fit(dataset.Idade)
x = np.linspace(min(dataset.Idade), max(dataset.Idade), 100)
p = norm.pdf(x, mean, sigma)
plt.plot(x, p, 'k')
plt.title('Fit results: mean = %.2f,  sigma = %.2f' %(mean, sigma))
plt.show()
