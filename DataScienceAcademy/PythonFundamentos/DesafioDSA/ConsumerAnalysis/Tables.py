# Importing required libraries
import pandas as pd
import numpy as np
from scipy.stats import mode
# ignoring all warnings
import warnings
warnings.filterwarnings("ignore")

# Loading original dataset
FileName = "data.json"
dataset = pd.read_json(FileName, orient = "records")
consumers = dataset.Login.unique()
allItems = dataset.loc[:,'Nome do Item'].unique()

# Identify first 5 buyers by the net value of transactions and
# list their Login, #purchases, mean price of transaction, popular items
#
tableBuyers = pd.DataFrame(columns = ['Login', 'Number_Purchases', 'Mean_Value', 'Net_Value', 'Most_Pop_Item'])
#
for consumer in consumers:
	consumerTransactions = dataset[dataset.Login == consumer]
	items = consumerTransactions.loc[:,'Nome do Item']
	mostPopItem = mode(items)
	#
	netValueAllTransactions = np.sum(consumerTransactions.Valor)
	numberOfPurchases = np.count_nonzero(consumerTransactions.Valor)
	meanValueTransaction = netValueAllTransactions/numberOfPurchases
	consumerInfo = pd.DataFrame({'Login':consumer,
								 'Net_Value':netValueAllTransactions,
								 'Number_Purchases':numberOfPurchases,
								 'Mean_Value':meanValueTransaction,
								 'Most_Pop_Item':mostPopItem[0][0]},
								 index = [0])
	#
	tableBuyers = tableBuyers.append(consumerInfo, ignore_index = False)
#
tableBuyers = tableBuyers.sort_values(by = 'Net_Value', ascending = 0)
tableBuyers.to_csv(r'Table_Buyers.csv', index = None, header = True)
# Print result
result = pd.read_csv('Table_Buyers.csv')
print(result.head(5))

# Identify the top 5 popular items using the number of sold units
# and list its ID, name, number of sold units, unitary price, net value
#
tableItems = pd.DataFrame(columns = ['Item_ID', 'Item_Name', 'Number_Purchases', 'Item_Price', 'Net_Value'])
#
for item in allItems:
	itemPurchases = dataset[dataset['Nome do Item'] == item]
	itemID = itemPurchases.loc[:,'Item ID'].unique()[0]
	itemPrice = itemPurchases.Valor.unique()[0]
	netValue = np.sum(itemPurchases.Valor)
	numberOfPurchases = np.count_nonzero(itemPurchases.Valor)
	itemInfo = pd.DataFrame({'Item_ID':itemID,
							 'Item_Name':item,
							 'Number_Purchases':numberOfPurchases,
							 'Item_Price':itemPrice,
							 'Net_Value':netValue},
							 index = [0])
	#
	tableItems = tableItems.append(itemInfo, ignore_index = False)
#
tableItems = tableItems.sort_values(by = 'Number_Purchases', ascending = 0)
tableItems.to_csv(r'Table_Items.csv', index = None, header = True)
# Print result
result = pd.read_csv('Table_Items.csv')
print(result.head(5))

# Identify the top 5 popular items using the net value
# and list its ID, name, number of sold units, unitary price, net value
#
tableItems = tableItems.sort_values(by = 'Net_Value', ascending = 0)
tableItems.to_csv(r'Table_Items_NetValue.csv', index = None, header = True)
result = pd.read_csv('Table_Items_NetValue.csv')
print(result.head(5))

