# this function was designed to build a list
# with unique values from a list with a large number of
# each value and return the number of times the
# repeated items are present
def getUniqueValuesList(someList):
    timesPerItem = dict.fromkeys(someList, 1)
    uniqueValuesList = []
    for item in someList:
        if (item in uniqueValuesList): timesPerItem[item] += 1
        else: uniqueValuesList.append(item)
    return( [uniqueValuesList, timesPerItem] )


class SelectionSort(object):
	def sort(self, data):
		[data,times] = getUniqueValuesList(data)
		minor = 0
		for j in range(0,len(data)-1):
			minor = data[j]
			for i in range(j,len(data)):
				minor = min(data[i],minor)
			data.remove(minor)
			data.insert(j,minor)
		for value,times in times.items():
			j = data.index(value)
			for i in range(1,times):
				data.insert(j,value)
		return(data)

from nose.tools import assert_equal, assert_raises

class TestSelectionSort(object):
	def test_selection_sort(self, func):
		print('None input')
		assert_raises(TypeError, func, None)

		print('Input vazio')
		assert_equal(func([]), [])

		print('Um elemento')
		assert_equal(func([5]), [5])

		print('Dois ou mais elementos')
		data = [5, 1, 7, 2, 6, -3, 5, 7, -10]
		assert_equal(func(data), sorted(data))

		print('Sua solução foi executada com sucesso! Parabéns!')

def main():
	test = TestSelectionSort()
	try:
		selection_sort = SelectionSort()
		test.test_selection_sort(selection_sort.sort)
	except NameError:
		pass

if __name__ == '__main__':
	main()
