import math
class PrimeGenerator(object):
	def generate_primes(self, max_num):
		array = [False, False, True] + [None for i in range(3, max_num)]
		for i in array:
			if i == None:
				i = array.index(i)
				for j in range(2,i):
					if i%j == 0: 
						array[i] = False
						break
					else:
						if (j==i-1):
							array[i] = True
							self._cross_off(array,i)
		return(array)

	def _cross_off(self, array, prime):
		n = 2
		while True:
			try:
				N = n*prime
				array[N] = False
				n += 1
			except IndexError:
				break
		
from nose.tools import assert_equal, assert_raises
class TestMath(object):
	def test_generate_primes(self):
		prime_generator = PrimeGenerator()
		assert_raises(TypeError, prime_generator.generate_primes, None)
		assert_raises(TypeError, prime_generator.generate_primes, 98.6)
		assert_equal(prime_generator.generate_primes(20), [False, False, True, True, False, True, False, True, False, False, False, True, False, True, False, False, False, True, False, True])
		print('Sua solução foi executada com sucesso! Parabéns!')

def main():
	test = TestMath()
	test.test_generate_primes()

if __name__ == '__main__':
	main()
