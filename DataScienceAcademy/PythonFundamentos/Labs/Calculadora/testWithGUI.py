# Algumas informações básicas obtidas aqui:
# https://www.devmedia.com.br/tkinter-interfaces-graficas-em-python/33956

# rodar com sudo python3 test_sum_with_gui.py
# para usar as fontes corretas

from tkinter import *

class calculator:
	def __init__(self, master=None):
		self.root = Tk()
		self.root.geometry("360x420")
		self.root.title("Calculadora")
		self.root.resizable(0,0)
		self.expressionView(master)
		self.expressionInput(master)
		self.evalExpression(master)
		self.numbers(master)
		self.operations(master)

	def numbers(self,master):
		self.alg1 = Button(master, text = "1",width = 10, command=lambda:self.input.insert(END,"1"))
		self.alg1.grid(row=2,column=0,sticky=EW)
		self.alg2 = Button(master, text = "2",width = 10, command=lambda:self.input.insert(END,"2"))
		self.alg2.grid(row=2,column=1,sticky=EW)
		self.alg3 = Button(master, text = "3",width = 10, command=lambda:self.input.insert(END,"3"))
		self.alg3.grid(row=3,column=0,sticky=EW)
		self.alg4 = Button(master, text = "4",width = 10, command=lambda:self.input.insert(END,"4"))
		self.alg4.grid(row=3,column=1,sticky=EW)
		self.alg5 = Button(master, text = "5",width = 10, command=lambda:self.input.insert(END,"5"))
		self.alg5.grid(row=4,column=0,sticky=EW)
		self.alg6 = Button(master, text = "6",width = 10, command=lambda:self.input.insert(END,"6"))
		self.alg6.grid(row=4,column=1,sticky=EW)
		self.alg7 = Button(master, text = "7",width = 10, command=lambda:self.input.insert(END,"7"))
		self.alg7.grid(row=5,column=0,sticky=EW)
		self.alg8 = Button(master, text = "8",width = 10, command=lambda:self.input.insert(END,"8"))
		self.alg8.grid(row=5,column=1,sticky=EW)
		self.alg9 = Button(master, text = "9",width = 10, command=lambda:self.input.insert(END,"9"))
		self.alg9.grid(row=6,column=0,sticky=EW)
		self.alg0 = Button(master, text = "0",width = 10, command=lambda:self.input.insert(END,"0"))
		self.alg0.grid(row=6,column=1,sticky=EW)
		self.deci = Button(master, text = ",",width = 10, command=lambda:self.input.insert(END,"."))
		self.deci.grid(row=6,column=2,sticky=EW)

	def operations(self,master):
		self.soma = Button(master, text = "+",width = 10, command=lambda:self.input.insert(END,"+"))
		self.soma.grid(row=2,column=2,sticky=EW)
		self.subt = Button(master, text = "-",width = 10, command=lambda:self.input.insert(END,"-"))
		self.subt.grid(row=3,column=2,sticky=EW)
		self.mult = Button(master, text = "x",width = 10, command=lambda:self.input.insert(END,"x"))
		self.mult.grid(row=4,column=2,sticky=EW)
		self.divi = Button(master, text = "/",width = 10, command=lambda:self.input.insert(END,"/"))
		self.divi.grid(row=5,column=2,sticky=EW)

	def expressionView(self,master):
		self.viewer = Listbox(master, font = "Roboto 20", height = 5) # visor dos cálculos realizados
		self.viewer.grid(row=0,column=0,columnspan=3,sticky=EW) # posição do visor: acima e à esquerda
		self.scrollbar = Scrollbar(master, orient="vertical", command=self.viewer.yview)
		self.scrollbar.grid(row=0,column=3,sticky=W+N+S)
		self.viewer.config(yscrollcommand=self.scrollbar.set)

	def expressionInput(self,master):
		self.input = Entry(master, font = "Roboto-Bold 20") # espaço em branco para receber as instruções
		self.input.grid(row=1,column=0,columnspan=3,sticky=EW) # usado para adicionar o objeto à instância do Tkinter

	def evalExpression(self,master):
		self.evaluate = Button(master, text="=", command=self.callBackButton, width = 10) # botão de igual
		self.evaluate.grid(row=7,column=0,columnspan=4,sticky=EW)
		self.root.bind("<Return>", self.callBack) # obter string apenas quando for apertado Enter

	def callBack(self,event):
		self.callBackButton()

	def callBackButton(self):
		if self.input.get() != '':
			self.expression = self.input.get()
			self.viewer.insert(END, self.expression)
			self.input.delete(0,END)
		#print(type(expression)) # for tests

	def start(self):
		self.root.mainloop()

calculator().start()
